import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

/*
  Generated class for the ServerResponseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServerResponseProvider {
  API_URL: any;
  constructor(public http: Http) {
    this.API_URL = "http://wecheck.co.za/rewards/index.php/api/scan_qr_code?"
    console.log('Hello ServerResponseProvider Provider');
  }
  getqrcodedata(param): Observable<any> {
    const url: string = `${this.API_URL}` + param;
    const headers = new Headers({
      'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT', 'Content-Type': 'application/json', 'Accept': 'application/json'
    });
    const options = new RequestOptions({ headers: headers });
    return this.http.post(url, options)
      .map(res => <any>res.json());

  }

  getmyrewards(param): Observable<any> {
    const url: string = `${param}`;
    const headers = new Headers({
      'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT', 'Content-Type': 'application/json', 'Accept': 'application/json'
    });
    const options = new RequestOptions({ headers: headers });
    return this.http.post(url, options)
      .map(res => <any>res.json());

  }

  rewardspoints(param): Observable<any> {
    const url: string = `${param}`;
    const headers = new Headers({
      'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT', 'Content-Type': 'application/json', 'Accept': 'application/json'
    });
    const options = new RequestOptions({ headers: headers });
    return this.http.post(url, options)
      .map(res => <any>res.json());

  }

  claimrewards(param): Observable<any> {
    const url: string = `${param}`;
    const headers = new Headers({
      'Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT', 'Content-Type': 'application/json', 'Accept': 'application/json'
    });
    const options = new RequestOptions({ headers: headers });
    return this.http.post(url, options)
      .map(res => <any>res.json());

  }
}
