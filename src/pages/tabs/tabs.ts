import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HomeTab, QRTab, SerchTab, MsgTab, MapTab } from '../';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tab1Root: any = HomeTab;
  tab2Root: any = QRTab;
  tab3Root: any = SerchTab;
  tab4Root: any = MsgTab;
  tab5Root: any = MapTab;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
