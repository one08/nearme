import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

/**
 * Generated class for the PopupimagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popupimage',
  templateUrl: 'popupimage.html',
})
export class PopupimagePage {

  popimage:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
    debugger;
    this.popimage=Object.assign({},this.navParams.data.img);

  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopupimagePage');
  }
  closepopup() {
    this.viewCtrl.dismiss(false);
  }

}
