import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopupimagePage } from './popupimage';

@NgModule({
  declarations: [
    PopupimagePage,
  ],
  imports: [
    IonicPageModule.forChild(PopupimagePage),
  ],
})
export class PopupimagePageModule {}
