import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ServerResponseProvider } from '../../providers/server-response/server-response';
import { NativeStorage } from '@ionic-native/native-storage';

/**
 * Generated class for the QrcodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-qrcode',
  templateUrl: 'qrcode.html',
})
export class QrcodePage {
  userid: any;
  constructor(public navCtrl: NavController, private barcodeScanner: BarcodeScanner,
    public navParams: NavParams, public alertCtrl: AlertController,
    public serverresp: ServerResponseProvider, private nativeStorage: NativeStorage) {

  }


  ionViewDidLoad() {
    //(window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
    console.log('ionViewDidLoad QrcodePage');

  }
  ionViewWillEnter() {
    this.nativeStorage.getItem('loginvalue').then((data) => {
      console.log(data);
      this.userid = data.id;
      this.scanbarcode();
    }, (error) => {
      // this.navCtrl.setRoot('SignInPage');
      console.error(error)
    });


  }
  scanbarcode() {
    this.barcodeScanner.scan().then(barcodeData => {
      var data = (barcodeData.text);
      try {
        barcodeData = JSON.parse(barcodeData.text);
      } catch (e) {
        this.unknownQRCode();
        console.log('qrcode note metch catch block', e);
      }
      var keys = barcodeData;
      if (!keys.hasOwnProperty('id') || !keys.hasOwnProperty("name") || !keys.hasOwnProperty("email") && !keys.hasOwnProperty("venue")) {
        this.unknownQRCode();
      }
      else {
        let marchent_data = (JSON.parse(data));
        let marchent_id = marchent_data.id
        /* var url = "http://wecheck.co.za/rewards/index.php/api/scan_qr_code?" +
          "user_id=" + user.id + "&" +
          "user_name=" + user.name + "&" +
          "merchant_id=" + marchent_id; */
        let param = "user_id=" + this.userid + "&" + "user_name=" + 'mike pablo' + "&" + "merchant_id=" + marchent_id;
        this.serverresp.getqrcodedata(param).subscribe((resp) => {
          //console.log('server resp', resp.data.venue_name);
          console.log('server resp', resp.venue_name);
          this.presentConfirm(resp);
        }, (err) => {
          console.log('server err', err);
        });
      }
    }).catch(err => {
      console.log('Error', err);
    });

  }

  unknownQRCode() {
    let alert = this.alertCtrl.create({
      title: 'Unknown qr code',
      message: 'Do you want to buy this book?',
      cssClass: 'alertDanger',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Rescan',
          handler: () => {
            this.scanbarcode();
          }
        }
      ]
    });
    alert.present();
  }






  presentConfirm(resp) {
    var resp = resp;
    let alert = this.alertCtrl.create({
      title: '(' + resp.venue_name + ') Congratulations!',
      message: 'You have earned ' + resp.earned_points + ' points. \nPlease scan QR code again' + (resp.repeat_hours > 0 ? " after " + resp.repeat_hours + " hours" : "") + " to earn more points.",
      cssClass: 'alertDanger',
      buttons: [
        {
          text: 'I Got It!',
          handler: () => {
            console.log('Buy clicked');
            this.navCtrl.push('RewardsPage');
          }
        }
      ]
    });
    alert.present();
  }






  ionViewWillLeave() {

  }



}
