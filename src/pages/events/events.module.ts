import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventsPage } from './events';
import { SharedModule } from '../../shared.module';

@NgModule({
  declarations: [
    EventsPage,
  ],
  imports: [
    IonicPageModule.forChild(EventsPage),
    SharedModule
  ],
  exports: [
    EventsPage
  ]
})
export class EventsPageModule { }
