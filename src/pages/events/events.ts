import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { BasePage } from '../base-page/base-page';

/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage extends BasePage {
  //private events: Event[] = [];

  constructor(injector: Injector) {
    super(injector);

  }
  enableMenuSwipe() {
    return true;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EventsPage');
  }
  goToPlaces() {
    this.navigateTo('EventDetailsPage');
  }

}
