import { Component, Injector, Renderer } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';
import { BasePage } from '../base-page/base-page';
import Parse from 'parse';
import { Slide } from '../../providers/slide';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { BrowserTab } from '@ionic-native/browser-tab';
import { ImgLoader } from 'ionic-image-loader';
import { Category } from '../../providers/categories';
import { Place } from '../../providers/place-service';
import { GeolocationOptions, Geolocation } from '@ionic-native/geolocation';



@IonicPage()
@Component({
  selector: 'page-home-page',
  templateUrl: 'home-page.html',
})
export class HomePage extends BasePage {

  title = 'app works!';
  result = '';


  protected slides: Slide[] = [];

  protected featuredPlaces: Place[] = [];
  protected newPlaces: Place[] = [];
  protected randomPlaces: Place[] = [];
  protected nearbyPlaces: Place[] = [];

  protected categories: Category[] = [];

  private randomParams: any = {};

  starCount:any=5;

  private location: any;

  constructor(injector: Injector,
    private events: Events,
    private geolocation: Geolocation,
    private placeService: Place,
    private inAppBrowser: InAppBrowser,
    private browserTab: BrowserTab,
    private renderer: Renderer
    ) {
    super(injector);
  }
   //kk
  // getHomeTimeline(){
  //   debugger;
  //   this.twitter.get(
  //     'https://api.twitter.com/1.1/statuses/home_timeline.json',
  //     {
  //       count: 5
  //     },
  //     {
  //       consumerKey: 'AKOynaFWbg2WFAFXxIeFHqX0A ',
  //       consumerSecret: 'X7yVdmVIR8ELIvX3SMhfPFcmIrzeIXcpUKZd5tCTzXuvVQ9iph '
  //     },
  //     {
  //       token: '1054367485678551042-CYnCUSVDTcMTaYGomD1hj8oipNjySQ ',
  //       tokenSecret: 'bck7A1aLkS1UbvOe2Q0CLyhkqWZyv6xhJoDRZ9lyGShUO'
  //     }
  // ).subscribe((res)=>{
  //     this.result = res.json().map(tweet => tweet.text);
  // });
  // }


  enableMenuSwipe(): boolean {
    return true;
  }

  ionViewDidLoad() {
    this.showLoadingView();
    this.loadData();
    this.loadNearbyPlaces();
  }

  onImageLoad(imgLoader: ImgLoader) {
    this.renderer.setElementClass(imgLoader.element, 'fade-in', true);
  }

  onReload(refresher = null) {
    this.refresher = refresher;
    this.loadData();
    this.loadNearbyPlaces();
  }

  async loadData() {

    try {

      const data: any = await Parse.Cloud.run('getHomePageData');

      this.randomPlaces = data.randomPlaces;
      this.newPlaces = data.newPlaces;
      this.featuredPlaces = data.featuredPlaces;
      this.categories = data.categories;
      this.slides = data.slides;

      this.onRefreshComplete();
      this.showContentView();

    } catch (error) {

      this.showErrorView();
      this.onRefreshComplete();

      this.translate.get('ERROR_NETWORK')
        .subscribe(str => this.showToast(str));

      if (error.code === 209) {
        this.events.publish('user:logout');
      }

    }

  }

  loadMoreRandomPlaces() {

    Parse.Cloud.run('getRandomPlaces').then((places: Place[]) => {

      for (const place of places) {
        this.randomPlaces.push(place);
      }

      this.onRefreshComplete();

    }, () => {
      this.onRefreshComplete();
      this.translate.get('ERROR_NETWORK').subscribe(str => this.showToast(str));
    });

  }

  async loadNearbyPlaces() {

    try {
      const options: GeolocationOptions = {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 60000
      };

      const pos = await this.geolocation.getCurrentPosition(options);
      this.location = pos.coords;

      this.nearbyPlaces = await this.placeService.load({
        location: this.location,
        limit: 10
      });

    } catch (err) {
      console.warn(err);
    }

  }

  onLoadMore(infiniteScroll) {
    this.infiniteScroll = infiniteScroll;
    this.randomParams.page++;
    this.loadMoreRandomPlaces();
  }
  fixsectionClick(){
    this.sectionClick(this.featuredPlaces[0]);
  }

  sectionClick(place, param2?) {
    debugger;
    console.log(place, param2);
    // place.image._url = 'assets/img/detailpage-bg.png';
    // place.description = 'In 1868, the officially recognized year celebrated as the "birth" of Sapporo, the new Meiji government concluded that the existing administrative center of Hokkaido, which at the time was the port of Hakodate, was in an unsuitable location for defense and further development of the island. As a result, it was determined that a new capital on the Ishikari Plain should be established.';
     place.distance = 20;
     place.unit = 'km';
    // place.opened = true;
     place.fromTime = '12:00';
     place.toTime = '23:00';
     place.tag = 'Asian Food';
     place.name = place.title;
     place.starCount = 3;
     console.clear();
     console.log(place);
    this.navigateTo('PlaceDetailPage', { place: place });
  }

  onSlideTouched(slide: Slide) {
    if (slide.url) {
      this.openUrl(slide.url);
    } else if (slide.place) {
      this.navigateTo('PlaceDetailPage', { place: slide.place });
    } else {
      // no match...
    }
  }

  openUrl(link) {
    this.browserTab.isAvailable().then((isAvailable: boolean) => {

      if (isAvailable) {
        this.browserTab.openUrl(link);
      } else {
        this.inAppBrowser.create(link, '_system');
      }

    }).catch(e => console.log(e));
  }

}
