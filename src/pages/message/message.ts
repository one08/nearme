import { Component, Injector } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { BasePage } from '../base-page/base-page';

/**
 * Generated class for the MessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


 
@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage extends BasePage {
  
  enableMenuSwipe(): boolean {
    return false;
  }

  constructor(injector: Injector) {
    super(injector);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagePage');
  }

}
