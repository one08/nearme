import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { ServerResponseProvider } from '../../providers/server-response/server-response';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the RewardsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rewards',
  templateUrl: 'rewards.html',
})

export class RewardsPage {
  public isErrorViewVisible: boolean;
  public isEmptyViewVisible: boolean;
  public isContentViewVisible: boolean;
  public isLoadingViewVisible: boolean;

  public loader: any;
  userid: any;
  myrewards: any;
  shownGroup: any;
  showdiv: boolean = false;
  rewards: any = [];
  reward: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public nativeStorage: NativeStorage, public serverresp: ServerResponseProvider, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController, public translate: TranslateService) {
    this.nativeStorage.getItem('loginvalue').then((data) => {
      console.log(data);
      this.userid = data.id;
      this.getmyrewards();
    }, (error) => {
      // this.navCtrl.setRoot('SignInPage');
      console.error(error)
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RewardsPage');
  }

  getmyrewards() {
    this.showLoadingView();
    var venue_list_url = 'http://wecheck.co.za/rewards/index.php/api/get_venue_list?' + "user_id=" + this.userid;
    this.serverresp.getmyrewards(venue_list_url).subscribe((resp) => {
      this.myrewards = resp;
      this.dismissLoadingView();
    }, (err) => {
      console.log(err);
      this.dismissLoadingView();
    })
  }




  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
      this.showdiv = false;
    } else {
      this.shownGroup = group;
      this.getmyrewardsdetails(this.shownGroup.merchant_id);
      this.showdiv = true;
    }
  }
  isGroupShown(group) {
    return this.shownGroup === group;
  };
  getmyrewardsdetails(merchant_id) {
    this.showLoadingView();
    var reward_url = 'http://wecheck.co.za/rewards/index.php/api/get_reward_data?' +
      "user_id=" + this.userid +
      "&merchant_id=" + merchant_id;
    this.serverresp.rewardspoints(reward_url).subscribe((data) => {
      this.rewards = data.rewards;
      this.dismissLoadingView();
    }, (e) => {
      console.log('error' + e);
      this.dismissLoadingView();
    })
  }

  rewardClicked(reward, venue) {
    this.reward = {};
    this.reward.id = reward._id;
    this.reward.merchant_id = reward.merchant_id;
    this.reward.title = reward.title;
    this.reward.venue_name = venue.venue_name;
    let alert = this.alertCtrl.create({
      title: 'Enter Staff Code',
      inputs: [
        {
          name: 'staffcode',
          placeholder: 'Please pass your device to a staff member'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log(data);
          }
        },
        {
          text: 'Ok',
          handler: data => {
            console.log(data);
            this.reward.staffcode = data.staffcode;
            if (!this.reward.staffcode) {
              //don't allow the user to close unless he enters staff code
            } else {
              this.CallClaimRewardService();
            }
          }
        }
      ]
    });
    alert.present();

  }

  CallClaimRewardService() {
    let url = 'http://wecheck.co.za/rewards/index.php/api/claim_reward?' + "user_id=" + this.userid +
      "&merchant_id=" + this.reward.merchant_id +
      "&reward_id=" + this.reward.id +
      "&staff_code=" + this.reward.staffcode;

    this.serverresp.claimrewards(url).subscribe((resp) => {
      console.log('reward claim', resp);
      if (resp.success) {
        let mess = '(' + this.reward.venue_name + ') Congratulations, You have claimed a' + this.reward.title + ' ' + 'info';
        this.presentAlert(mess);
        this.updateRewardBalance();
      } else {
        var message = "";
        switch (resp.failed_reason) {
          case 1:
            message = "Invalid Staff Code";
            break;
          case 2:
            message = "Points Balance is not enough";
            break;
          default:
            message = "Please check your connection and try again!"
        }
        let mes = '(' + this.reward.venue_name + ') Error,' + message + ' ' + "error";
        this.presentAlert(mes);

      }
    })

  }
  updateRewardBalance() {
    this.getmyrewards();
  }
  presentAlert(mess) {
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: mess,
      buttons: ['Dismiss']
    });
    alert.present();
  }


  showLoadingView() {
    return new Promise((resolve) => {
      this.translate.get('LOADING').subscribe((loadingText: string) => {

        this.loader = this.loadingCtrl.create({
          content: `<p class="bold">${loadingText}</p>`
        });
        this.loader.present();
        resolve();
      });
    });
  }

  dismissLoadingView() {
    if (this.loader)
      this.loader.dismiss();
    this.loader = false;
  }

}
