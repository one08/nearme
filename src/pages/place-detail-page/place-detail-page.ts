import { IonicPage, ActionSheetController, Slides } from 'ionic-angular';
import { Component, Injector, Renderer, ViewChild } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { Place } from '../../providers/place-service';
import { Preference } from '../../providers/preference';
import { CallNumber } from '@ionic-native/call-number';
import { Geolocation, GeolocationOptions } from '@ionic-native/geolocation';
//import { InAppBrowser } from '@ionic-native/in-app-browser';
//import { BrowserTab } from '@ionic-native/browser-tab';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { SocialSharing } from '@ionic-native/social-sharing';
import { BasePage } from '../base-page/base-page';
import { User } from '../../providers/user-service';
import { ImgLoader } from 'ionic-image-loader';
import { Review } from '../../providers/review-service';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
// import { PopupimagePage } from '../popupimage/popupimage';

import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';

declare var $: any;

@IonicPage()
@Component({
  selector: 'page-place-detail-page',
  templateUrl: 'place-detail-page.html'
})
export class PlaceDetailPage extends BasePage {

  private images = [];
  private place: Place;
  private rating: number = 0;
  private isLiked: boolean = false;
  private isStarred: boolean = false;
  private location: any;
  private unit: string;
  gallery: any;
  @ViewChild(Slides) slides: Slides;
  private openSlide: boolean = false;
  tabs = [{ index: 1, name: 'Info' }, { index: 2, name: 'Media' }, { index: 3, name: 'Menu' }, { index: 4, name: 'Social' }];
  tabClickedIndex = 1;
  tabClickedName = 'Info';
  private reviews: Review[] = [];
  constructor(injector: Injector,
    private renderer: Renderer,
    //private platform: Platform,
    private placeService: Place,
    private modalCtrl: ModalController,
    private preference: Preference,
    private callNumber: CallNumber,
    private geolocation: Geolocation,
    // private inAppBrowser: InAppBrowser,
    // private browserTab: BrowserTab,
    private reviewService: Review,
    private launchNavigator: LaunchNavigator,
    private socialSharing: SocialSharing,
    public actionSheetCtrl: ActionSheetController,
    private cameraservice: Camera,
    public imagepick: ImagePicker,
    private document: DocumentViewer
  ) {

    
    super(injector);
    // $(document).ready(() => {
		// 	this.getAllSocialFeeds();
    // });
    
    this.place = this.navParams.get('place');
    this.unit = this.preference.unit;
    this.images = [];
    this.gallery = [{
      src: 'assets/img/home-item-1.jpeg'
    },
    {
      src: 'assets/img/home-item-2.jpg'

    }]
    if (this.place) {

      this.rating = this.place.rating;

      this.loadLocation();

      if (User.getCurrentUser()) {
        this.checkIfIsLiked();
        this.checkIfIsStarred();
      }

      this.loadReviews();
      // this.images.push({
      //   _url: 'assets/img/detailpage-bg.png',
      //   location_desc: 'In 1868, the officially recognized year celebrated as the "birth" of Sapporo, the new Meiji government concluded that the existing administrative center of Hokkaido, which at the time was the port of Hakodate, was in an unsuitable location for defense and further development of the island. As a result, it was determined that a new capital on the Ishikari Plain should be established.'
      // });
      if (this.place.image) {

        this.images.push(this.place.image);
        console.log('images', this.images);
      }
      /*
            
            if (this.place.imageTwo) {
              this.images.push(this.place.imageTwo);
            }
      
            if (this.place.imageThree) {
              this.images.push(this.place.imageThree);
            }
      
            if (this.place.imageFour) {
              this.images.push(this.place.imageFour);
            }
            */
    }
  }



  tabclicked(index) {
    debugger;
    this.tabClickedIndex = index;
    this.tabClickedName = this.tabs[index - 1].name;
    this.openSlide = false;

    debugger;
    if(index==4)
    {
      setTimeout(()=>{
        this.getAllSocialFeeds();
      },100);
      
    }

  }

  getAllSocialFeeds = () => {
		var initialQuery = "#marvel";
		initialQuery = initialQuery.replace(" ", "");
		var queryTags = initialQuery.split(",");
		(<any>$('.social-feed-container')).socialfeed({
			// FACEBOOK
			facebook: {
				accounts: ['@teslamotors','!teslamotors'],
				limit: 2,
				access_token: '150849908413827|a20e87978f1ac491a0c4a721c961b68c'
			},
			// GOOGLEPLUS
			google: {
				accounts: queryTags,
				limit: 0,
				access_token: 'AIzaSyDAelFmJhg6BSUbSLe8UT7s-G53tL4_KRg'
			},
			// Twitter
			twitter: {
				accounts: queryTags,
				limit: 2,
				consumer_key: 'qzRXgkI7enflNJH1lWFvujT2P', // make sure to have your app read-only
				consumer_secret: '8e7E7gHuTwyDHw9lGQFO73FcUwz9YozT37lEvZulMq8FXaPl8O', // make sure to have your app read-only
			},
			// VKONTAKTE
			vk: {
				accounts: queryTags,
				limit: 0,
				source: 'all'
			},
			// INSTAGRAM
			// instagram: {
			// 	accounts: queryTags,
			// 	limit: 2,
			// 	client_id: '88b4730e0e2c4b2f8a09a6184af2e218',
			// 	access_token: ''
			// },
		    // GENERAL SETTINGS
		    length:400,
		    show_media:true,
		    media_min_width: 200,
		    update_period: 5000,
		    template_html:                                 
		    `<div class="social-feed-element {{? !it.moderation_passed}}hidden{{?}}" dt-create="{{=it.dt_create}}" social-feed-id = "{{=it.id}}"> \
			<div class='content'> \
				<a class="pull-left" href="{{=it.author_link}}" target="_blank"> \
					<img class="media-object" src="{{=it.author_picture}}"> \
				</a> \
				<div class="media-body"> \
					<p> \
						<i class="fa fa-{{=it.social_network}}"></i> \
						<span class="author-title">{{=it.author_name}}</span> \
						<span class="muted pull-right"> {{=it.time_ago}}</span> \  
					</p> \
					<div class='text-wrapper'> \
						<p class="social-feed-text">{{=it.text}} <a href="{{=it.link}}" target="_blank" class="read-button">read more</a></p> \
					</div> \
				</div> \
			</div> \
			{{=it.attachment}} \
		</div>`,
		    date_format: "ll",                              //String: Display format of the date attribute (see http://momentjs.com/docs/#/displaying/format/)
		    date_locale: "en",                              //String: The locale of the date (see: http://momentjs.com/docs/#/i18n/changing-locale/)
		    moderation: (content) => {                 //Function: if returns false, template will have class hidden
		        return  (content.text) ? content.text.indexOf('fuck') == -1 : true;
		    },
		    callback: () => {                          //Function: This is a callback function which is evoked when all the posts are collected and displayed
		        console.log("All posts collected!");
		    }
		});
	}


  enableMenuSwipe() {
    return false;
  }

  ionViewDidLoad() {

  }

  onImageLoad(imgLoader: ImgLoader) {
    this.renderer.setElementClass(imgLoader.element, 'fade-in', true);
  }

  async checkIfIsLiked() {
    try {
      const isLiked = await this.placeService.isLiked(this.place)
      this.isLiked = isLiked;
    } catch (err) {
      console.warn(err.message);
    }
  }

  async checkIfIsStarred() {
    try {
      const isStarred = await this.placeService.isStarred(this.place)
      this.isStarred = isStarred;
    } catch (err) {
      console.warn(err.message);
    }
  }

  async loadLocation() {
    try {

      const options: GeolocationOptions = {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 60000
      };

      let pos = await this.geolocation.getCurrentPosition(options);
      this.location = pos.coords;

    } catch (err) {
      console.warn(err);
    }
  }

  async loadReviews() {
    try {

      let reviews = await this.reviewService.load({ place: this.place, limit: 5 });
      this.reviews = reviews;

    } catch (err) {
      console.warn(err.message);
    }
  }

  openSignUpModal() {
    this.navigateTo('SignInPage');
  }

  openAddReviewModal() {
    let modal = this.modalCtrl.create('AddReviewPage', { place: this.place });
    modal.present();
  }

  onLike() {
    debugger;
    if (User.getCurrentUser()) {
      this.isLiked = true;
      this.placeService.like(this.place);
      this.showToast('Liked');
    } else {
      this.openSignUpModal();
    }
  }

  onRate() {
    if (User.getCurrentUser()) {
      this.openAddReviewModal();
    } else {
      this.openSignUpModal();
    }
  }

  async onShare() {
    try {
      await this.socialSharing.share(this.place.title, null, null, this.place.website)
    } catch (err) {
      console.warn(err)
    }
  }

  async onCall() {
    if (!this.place.phone) return;
    try {
      await this.callNumber.callNumber(this.place.phone, true)
    } catch (err) {
      console.warn(err)
    }
  }



  openUberUrl() {
    let options: LaunchNavigatorOptions = {
      start: '',
      app: this.launchNavigator.APP.UBER,
    };

    this.launchNavigator.navigate(this.place.title, options).then((success) => {
      console.log(success);
    }, () => {
      console.log('Error launching navigator')
    });

  }

  async goToMap() {

    try {

      const googleMaps = this.launchNavigator.APP.GOOGLE_MAPS;
      const appleMaps = this.launchNavigator.APP.APPLE_MAPS;

      const isGoogleMapsAvailable = await this.launchNavigator.isAppAvailable(googleMaps);
      const isAppleMapsAvailable = await this.launchNavigator.isAppAvailable(appleMaps);

      let app = null;

      if (isGoogleMapsAvailable) {
        app = this.launchNavigator.APP.GOOGLE_MAPS;
      } else if (isAppleMapsAvailable) {
        app = this.launchNavigator.APP.APPLE_MAPS;
      } else {
        app = this.launchNavigator.APP.USER_SELECT;
      }

      const options: LaunchNavigatorOptions = {
        app: app
      };

      const destination = [
        this.place.location.latitude,
        this.place.location.longitude
      ];

      await this.launchNavigator.navigate(destination, options);

    } catch (err) {
      console.warn(err);
    }

  }

  goToReviews() {
    this.navigateTo('ReviewsPage', this.place);
  }
  showPopup() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Pick Image',
      buttons: [{
        text: 'Take Image',
        handler: () => {
          this.takePicture();
        }
      },
      {
        text: 'From Gallery',
        handler: () => {

          //**************** */
          this.openImagePicker();
        }
      },
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          // console.log('Cancel clicked');
        }
      }
      ]
    });


    actionSheet.present();
  }

  onClickGallery(i) {
    this.slides.slideTo(i);
    this.openSlide = true;
    //kk
    debugger;
    // let obj:any=i;
    // const modal =this.modalCtrl.create(PopupimagePage,{img:obj})
    // modal.present();

  }
  off() {
    this.openSlide = false;
  }

  takePicture() {
    let options = {
      quality: 100,
      correctOrientation: true
    };
    this.cameraservice.getPicture(options).then((data) => {
      console.log(data);
    }, () => {

    });
  }
  openImagePicker() {
    let options = {
      maximumImagesCount: 1,
    }
    this.imagepick.getPictures(options)
      .then((results) => {
        if (results != '') {
          results.forEach((item) => {
            console.log(item);
            /* window.resolveLocalFileSystemURL(item, (fileEntry) => {
              fileEntry.getMetadata((metadata) => {
                console.log('size', metadata.size);
                if (metadata.size > 20971520) {
                  this.remotService.dismissLoader();
                  this.remotService.presentToast(' Please upload a file with size less than: ' + 20 + "MB");
                } else {
                  this.remotService.dismissLoader();
                  this.reduceImages(results).then(() => {
                    if (this.changeimageType == 'profile')
                      this.saveProfileImage();
                    else
                      this.addStatusImage();
                  });

                  //this.saveImageToArrayBypath(item);
                }
              });
            }); */
          })
        }

      }, () => {
        //console.log(err)
      });
  }

  async open() {

    // this.tabClickedIndex = index;
    // this.tabClickedName = this.tabs[index - 1].name;
    // this.openSlide = false;

    debugger;
    const options: DocumentViewerOptions = {
      title: 'My PDF'
    }

    var baseUrl = location.href.split("index.html#");

    this.document.viewDocument(baseUrl[0] + 'assets/pdf/sample.pdf', 'application/pdf', options);

  }
}
