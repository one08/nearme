cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "mx.ferreyra.callnumber.CallNumber",
    "file": "plugins/mx.ferreyra.callnumber/www/CallNumber.js",
    "pluginId": "mx.ferreyra.callnumber",
    "clobbers": [
      "call"
    ]
  },
  {
    "id": "cordova-plugin-document-viewer.SitewaertsDocumentViewer",
    "file": "plugins/cordova-plugin-document-viewer/www/sitewaertsdocumentviewer.js",
    "pluginId": "cordova-plugin-document-viewer",
    "clobbers": [
      "cordova.plugins.SitewaertsDocumentViewer",
      "SitewaertsDocumentViewer"
    ]
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-admob-sdk": "0.16.0",
  "cordova-android-play-services-gradle-release": "1.4.3",
  "cordova-android-support-gradle-release": "1.4.4",
  "mx.ferreyra.callnumber": "0.0.2",
  "cordova-plugin-document-viewer": "0.9.10"
};
// BOTTOM OF METADATA
});