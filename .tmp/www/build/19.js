webpackJsonp([19],{

/***/ 701:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventsPageModule", function() { return EventsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__events__ = __webpack_require__(728);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_module__ = __webpack_require__(405);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var EventsPageModule = (function () {
    function EventsPageModule() {
    }
    EventsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__events__["a" /* EventsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__events__["a" /* EventsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__shared_module__["a" /* SharedModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__events__["a" /* EventsPage */]
            ]
        })
    ], EventsPageModule);
    return EventsPageModule;
}());

//# sourceMappingURL=events.module.js.map

/***/ }),

/***/ 728:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__base_page_base_page__ = __webpack_require__(224);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the EventsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EventsPage = (function (_super) {
    __extends(EventsPage, _super);
    //private events: Event[] = [];
    function EventsPage(injector) {
        return _super.call(this, injector) || this;
    }
    EventsPage.prototype.enableMenuSwipe = function () {
        return true;
    };
    EventsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EventsPage');
    };
    EventsPage.prototype.goToPlaces = function () {
        this.navigateTo('EventDetailsPage');
    };
    EventsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-events',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\pages\events\events.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>\n\n      <span>\n\n        Events\n\n      </span>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="cards-bg">\n\n\n\n  <ion-card (tap)="goToPlaces()">\n\n    <div class="img-wr">\n\n      <div class="date">\n\n        <ion-icon name="ios-calendar-outline" style="background:none !important"></ion-icon><span>12 Jun 2018</span>\n\n      </div>\n\n      <img src="assets/img/bg.png" />\n\n    </div>\n\n\n\n    <ion-card-content>\n\n      <ion-card-title>\n\n        <h3>Nine Inch Nails Live</h3>\n\n        <div class="time">\n\n          <ion-icon name="ios-clock-outline" style="background:white"></ion-icon><span>12:00</span>-<span>23:00</span>\n\n        </div>\n\n        <div class="distance">\n\n          <ion-icon name="pin" style="background:white"></ion-icon>\n\n          <span>15km</span>\n\n        </div>\n\n      </ion-card-title>\n\n      <p class="descrip">\n\n        The most popular industrial group ever, and largely responsible for bringing the music to a mass audience.\n\n      </p>\n\n    </ion-card-content>\n\n\n\n    <!--  <ion-row no-padding class="big-btns">\n\n      <ion-col>\n\n        <button ion-button clear>\n\n          <div class="b-ic-wr">\n\n            <img src="assets/img/call.png">\n\n          </div>\n\n          <p>call</p>\n\n        </button>\n\n      </ion-col>\n\n      <ion-col text-center>\n\n        <button ion-button clear>\n\n          <div class="b-ic-wr">\n\n            <img src="assets/img/navigation.png">\n\n          </div>\n\n          <p>get directions</p>\n\n        </button>\n\n      </ion-col>\n\n      <ion-col text-right>\n\n        <button ion-button clear>\n\n          <div class="b-ic-wr">\n\n            <img src="assets/img/uber.png">\n\n          </div>\n\n          <p>uber</p>\n\n        </button>\n\n      </ion-col>\n\n    </ion-row> -->\n\n\n\n  </ion-card>\n\n  <ion-card>\n\n    <div class="img-wr">\n\n      <div class="date">\n\n        <ion-icon name="ios-calendar-outline" style="background:none !important"></ion-icon><span>12 Jun 2018</span>\n\n      </div>\n\n      <img src="assets/img/bg.png" />\n\n    </div>\n\n\n\n    <ion-card-content>\n\n      <ion-card-title>\n\n        <h3>Nine Inch Nails Live</h3>\n\n        <div class="time">\n\n          <ion-icon name="ios-clock-outline" style="background:white"></ion-icon><span>12:00</span>-<span>23:00</span>\n\n        </div>\n\n        <div class="distance">\n\n          <ion-icon name="pin" style="background:white"></ion-icon>\n\n          <span>15km</span>\n\n        </div>\n\n      </ion-card-title>\n\n      <p class="descrip">\n\n        The most popular industrial group ever, and largely responsible for bringing the music to a mass audience.\n\n      </p>\n\n    </ion-card-content>\n\n\n\n    <!--  <ion-row no-padding class="big-btns">\n\n      <ion-col>\n\n        <button ion-button clear>\n\n          <div class="b-ic-wr">\n\n            <img src="assets/img/call.png">\n\n          </div>\n\n          <p>call</p>\n\n        </button>\n\n      </ion-col>\n\n      <ion-col text-center>\n\n        <button ion-button clear>\n\n          <div class="b-ic-wr">\n\n            <img src="assets/img/navigation.png">\n\n          </div>\n\n          <p>get directions</p>\n\n        </button>\n\n      </ion-col>\n\n      <ion-col text-right>\n\n        <button ion-button clear>\n\n          <div class="b-ic-wr">\n\n            <img src="assets/img/uber.png">\n\n          </div>\n\n          <p>uber</p>\n\n        </button>\n\n      </ion-col>\n\n    </ion-row> -->\n\n\n\n  </ion-card>\n\n\n\n</ion-content>'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\pages\events\events.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injector"]])
    ], EventsPage);
    return EventsPage;
}(__WEBPACK_IMPORTED_MODULE_1__base_page_base_page__["a" /* BasePage */]));

//# sourceMappingURL=events.js.map

/***/ })

});
//# sourceMappingURL=19.js.map