webpackJsonp([7],{

/***/ 714:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignInPageModule", function() { return SignInPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sign_in_page__ = __webpack_require__(742);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_module__ = __webpack_require__(405);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SignInPageModule = (function () {
    function SignInPageModule() {
    }
    SignInPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sign_in_page__["a" /* SignInPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sign_in_page__["a" /* SignInPage */]),
                __WEBPACK_IMPORTED_MODULE_3__shared_module__["a" /* SharedModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__sign_in_page__["a" /* SignInPage */]
            ]
        })
    ], SignInPageModule);
    return SignInPageModule;
}());

//# sourceMappingURL=sign-in-page.module.js.map

/***/ }),

/***/ 742:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignInPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__base_page_base_page__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_service__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_storage__ = __webpack_require__(225);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






//import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
var SignInPage = (function (_super) {
    __extends(SignInPage, _super);
    function SignInPage(injector, events, userService, 
        // private fb: Facebook,
        viewCtrl, nativeStorage) {
        var _this = _super.call(this, injector) || this;
        _this.events = events;
        _this.userService = userService;
        _this.viewCtrl = viewCtrl;
        _this.nativeStorage = nativeStorage;
        _this.form = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
            username: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required),
            password: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required)
        });
        return _this;
        /* this.events.subscribe('user:login', () => {
          this.onCancel();
        }); */
    }
    SignInPage.prototype.enableMenuSwipe = function () {
        return false;
    };
    SignInPage.prototype.ionViewDidLoad = function () {
    };
    SignInPage.prototype.onCancel = function () {
        this.viewCtrl.dismiss();
    };
    SignInPage.prototype.onFacebookButtonTouched = function () {
        /* this.fb.login(['public_profile', 'user_friends', 'email'])
        .then((res: FacebookLoginResponse) => this.loggedIntoFacebook(res))
        .catch(e => console.log('Error logging into Facebook', e)); */
    };
    /* loggedIntoFacebook(res: FacebookLoginResponse) {
  
      console.log('Logged into Facebook', res);
  
      let expirationDate = new Date();
      expirationDate.setSeconds(expirationDate.getSeconds() + res.authResponse.expiresIn);
  
      let expirationDateFormatted = expirationDate.toISOString();
  
      var facebookAuthData = {
        id: res.authResponse.userID,
        access_token: res.authResponse.accessToken,
        expiration_date: expirationDateFormatted
      };
  
      this.showLoadingView().then(() => {
        this.userService.loginWithFacebook({ authData: facebookAuthData })
          .then(user => this.loggedViaFacebook(user))
          .catch(e => this.loginViaFacebookFailure(e));
      });
  
    } */
    SignInPage.prototype.loginViaFacebookFailure = function (error) {
        var _this = this;
        console.log('Error logging into Facebook', error);
        this.translate.get('ERROR_UNKNOWN').subscribe(function (str) { return _this.showToast(str); });
        this.showContentView();
    };
    SignInPage.prototype.loggedViaFacebook = function (user) {
        var _this = this;
        this.showContentView();
        var transParams = { username: user.name };
        this.translate.get('LOGGED_IN_AS', transParams)
            .subscribe(function (str) { return _this.showToast(str); });
        this.events.publish('user:login', user);
    };
    SignInPage.prototype.onSubmit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var message, user, transParams, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        debugger;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 6, , 7]);
                        if (!this.form.invalid) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.getTrans('INVALID_FORM')];
                    case 2:
                        message = _a.sent();
                        return [2 /*return*/, this.showToast(message)];
                    case 3: return [4 /*yield*/, this.showLoadingView()];
                    case 4:
                        _a.sent();
                        return [4 /*yield*/, this.userService.signIn(this.form.value)];
                    case 5:
                        user = _a.sent();
                        this.showContentView();
                        transParams = { username: user.username };
                        //this.translate.get('LOGGED_IN_AS', transParams).subscribe(str => this.showToast(str));
                        this.translate.get('LOGGED_IN_AS', transParams).subscribe(function (str) {
                            _this.showToast(str);
                        }, function () {
                        });
                        this.nativeStorage.setItem('loginvalue', { id: user.id, name: user.name }).then(function () {
                            console.log('Stored item!');
                        }, function (error) {
                            console.error('Error storing item', error);
                        });
                        this.events.publish('user:login', user);
                        return [3 /*break*/, 7];
                    case 6:
                        err_1 = _a.sent();
                        if (err_1.code === 101) {
                            this.translate.get('INVALID_CREDENTIALS')
                                .subscribe(function (str) { return _this.showToast(str); });
                        }
                        else {
                            this.translate.get('ERROR_NETWORK')
                                .subscribe(function (str) { return _this.showToast(str); });
                        }
                        this.showContentView();
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    SignInPage.prototype.goToSignUp = function () {
        this.navigateTo('SignUpPage');
    };
    SignInPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sign-in-page',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\pages\sign-in-page\sign-in-page.html"*/'<ion-header no-border>\n\n  <ion-navbar class="transparent" color="primary">\n\n    <ion-title>{{ \'LOGIN\' | translate }}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<!-- <ion-view class="pane"></ion-view> -->\n\n<ion-content padding text-center >\n\n\n\n  <img margin class="logo" src="./assets/img/logo-1.png" width="120">\n\n\n\n  <form [formGroup]="form" (ngSubmit)="onSubmit()" novalidate>\n\n\n\n    <ion-item class="transparent">\n\n      <ion-label stacked color="light">{{ "USERNAME" | translate }}</ion-label>\n\n      <ion-input type="text" formControlName="username"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item class="transparent">\n\n      <ion-label stacked color="light">{{ "PASSWORD" | translate }}</ion-label>\n\n      <ion-input type="password" formControlName="password"></ion-input>\n\n    </ion-item>\n\n\n\n    <div padding>\n\n      <button type="submit" color="light" ion-button block outline round>\n\n        {{ "LOGIN" | translate }}\n\n      </button>\n\n    </div>\n\n\n\n    <div padding>\n\n      <button type="button" color="light" ion-button icon-start block round (click)="onFacebookButtonTouched()">\n\n        <ion-icon name="logo-facebook" color="fb"></ion-icon>\n\n        {{ "FACEBOOK" | translate }}\n\n      </button>\n\n    </div>\n\n\n\n    <div padding>\n\n      <button type="button" class="bold" ion-button block clear color="light" (click)="goToSignUp()">\n\n        {{ "NO_ACCOUNT_YET_CREATE_ONE" | translate }}\n\n      </button>\n\n\n\n      <button type="button" class="bold" ion-button block clear color="light" (click)="navigateTo(\'ForgotPasswordPage\')">\n\n        {{ "FORGOT_PASSWORD" | translate }}\n\n      </button>\n\n    </div>\n\n\n\n  </form>\n\n</ion-content>'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\pages\sign-in-page\sign-in-page.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injector"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__providers_user_service__["a" /* User */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["A" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], SignInPage);
    return SignInPage;
}(__WEBPACK_IMPORTED_MODULE_2__base_page_base_page__["a" /* BasePage */]));

//# sourceMappingURL=sign-in-page.js.map

/***/ })

});
//# sourceMappingURL=7.js.map