webpackJsonp([10],{

/***/ 711:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RewardsPageModule", function() { return RewardsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__rewards__ = __webpack_require__(739);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RewardsPageModule = (function () {
    function RewardsPageModule() {
    }
    RewardsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__rewards__["a" /* RewardsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__rewards__["a" /* RewardsPage */]),
            ],
        })
    ], RewardsPageModule);
    return RewardsPageModule;
}());

//# sourceMappingURL=rewards.module.js.map

/***/ }),

/***/ 739:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RewardsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_native_storage__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_server_response_server_response__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(79);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RewardsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RewardsPage = (function () {
    function RewardsPage(navCtrl, navParams, nativeStorage, serverresp, alertCtrl, loadingCtrl, translate) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.nativeStorage = nativeStorage;
        this.serverresp = serverresp;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.translate = translate;
        this.showdiv = false;
        this.rewards = [];
        this.nativeStorage.getItem('loginvalue').then(function (data) {
            console.log(data);
            _this.userid = data.id;
            _this.getmyrewards();
        }, function (error) {
            // this.navCtrl.setRoot('SignInPage');
            console.error(error);
        });
    }
    RewardsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RewardsPage');
    };
    RewardsPage.prototype.getmyrewards = function () {
        var _this = this;
        this.showLoadingView();
        var venue_list_url = 'http://wecheck.co.za/rewards/index.php/api/get_venue_list?' + "user_id=" + this.userid;
        this.serverresp.getmyrewards(venue_list_url).subscribe(function (resp) {
            _this.myrewards = resp;
            _this.dismissLoadingView();
        }, function (err) {
            console.log(err);
            _this.dismissLoadingView();
        });
    };
    RewardsPage.prototype.toggleGroup = function (group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
            this.showdiv = false;
        }
        else {
            this.shownGroup = group;
            this.getmyrewardsdetails(this.shownGroup.merchant_id);
            this.showdiv = true;
        }
    };
    RewardsPage.prototype.isGroupShown = function (group) {
        return this.shownGroup === group;
    };
    ;
    RewardsPage.prototype.getmyrewardsdetails = function (merchant_id) {
        var _this = this;
        this.showLoadingView();
        var reward_url = 'http://wecheck.co.za/rewards/index.php/api/get_reward_data?' +
            "user_id=" + this.userid +
            "&merchant_id=" + merchant_id;
        this.serverresp.rewardspoints(reward_url).subscribe(function (data) {
            _this.rewards = data.rewards;
            _this.dismissLoadingView();
        }, function (e) {
            console.log('error' + e);
            _this.dismissLoadingView();
        });
    };
    RewardsPage.prototype.rewardClicked = function (reward, venue) {
        var _this = this;
        this.reward = {};
        this.reward.id = reward._id;
        this.reward.merchant_id = reward.merchant_id;
        this.reward.title = reward.title;
        this.reward.venue_name = venue.venue_name;
        var alert = this.alertCtrl.create({
            title: 'Enter Staff Code',
            inputs: [
                {
                    name: 'staffcode',
                    placeholder: 'Please pass your device to a staff member'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function (data) {
                        console.log(data);
                    }
                },
                {
                    text: 'Ok',
                    handler: function (data) {
                        console.log(data);
                        _this.reward.staffcode = data.staffcode;
                        if (!_this.reward.staffcode) {
                            //don't allow the user to close unless he enters staff code
                        }
                        else {
                            _this.CallClaimRewardService();
                        }
                    }
                }
            ]
        });
        alert.present();
    };
    RewardsPage.prototype.CallClaimRewardService = function () {
        var _this = this;
        var url = 'http://wecheck.co.za/rewards/index.php/api/claim_reward?' + "user_id=" + this.userid +
            "&merchant_id=" + this.reward.merchant_id +
            "&reward_id=" + this.reward.id +
            "&staff_code=" + this.reward.staffcode;
        this.serverresp.claimrewards(url).subscribe(function (resp) {
            console.log('reward claim', resp);
            if (resp.success) {
                var mess = '(' + _this.reward.venue_name + ') Congratulations, You have claimed a' + _this.reward.title + ' ' + 'info';
                _this.presentAlert(mess);
                _this.updateRewardBalance();
            }
            else {
                var message = "";
                switch (resp.failed_reason) {
                    case 1:
                        message = "Invalid Staff Code";
                        break;
                    case 2:
                        message = "Points Balance is not enough";
                        break;
                    default:
                        message = "Please check your connection and try again!";
                }
                var mes = '(' + _this.reward.venue_name + ') Error,' + message + ' ' + "error";
                _this.presentAlert(mes);
            }
        });
    };
    RewardsPage.prototype.updateRewardBalance = function () {
        this.getmyrewards();
    };
    RewardsPage.prototype.presentAlert = function (mess) {
        var alert = this.alertCtrl.create({
            title: '',
            subTitle: mess,
            buttons: ['Dismiss']
        });
        alert.present();
    };
    RewardsPage.prototype.showLoadingView = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.translate.get('LOADING').subscribe(function (loadingText) {
                _this.loader = _this.loadingCtrl.create({
                    content: "<p class=\"bold\">" + loadingText + "</p>"
                });
                _this.loader.present();
                resolve();
            });
        });
    };
    RewardsPage.prototype.dismissLoadingView = function () {
        if (this.loader)
            this.loader.dismiss();
        this.loader = false;
    };
    RewardsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-rewards',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\pages\rewards\rewards.html"*/'<!--\n\n  Generated template for the RewardsPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="icon-menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Rewards</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <ion-list class="rewards-list">\n\n    <ion-item *ngFor="let venue of myrewards" (tap)="toggleGroup(venue)">\n\n      <ion-avatar item-start>\n\n        <img [src]="venue.mprofile_pic" />\n\n      </ion-avatar>\n\n      <h2 class="app_text_color">{{venue.venue_name}}</h2>\n\n      <p class="my_reward_text">Your Points Earned : <span class="value">{{venue.points_balance}}</span></p>\n\n      <button ion-button clear item-end>\n\n        <ion-icon name="arrow-dropup" *ngIf="!isGroupShown(venue)"></ion-icon>\n\n        <ion-icon name="arrow-dropdown" *ngIf="isGroupShown(venue)"></ion-icon>\n\n      </button>\n\n    </ion-item>\n\n\n\n\n\n\n\n    <ion-item class="item-accordion child-item" *ngIf="showdiv">\n\n      <img *ngIf="rewards && rewards.length > 0" class="icon-img" src="assets/icon/icon-tap.png" />\n\n      <p class="note my_reward_text" *ngIf="rewards && rewards.length > 0">Tap the desired reward box to claim your\n\n        reward</p>\n\n      <p class="note my_reward_text" *ngIf="rewards && rewards.length == 0">No rewards founds.</p>\n\n\n\n      <div class="row item-grid padding_0" style="overflow-x: auto">\n\n        <div class="col col-50" (tap)="rewardClicked(reward, shownGroup)" *ngFor="let reward of rewards">\n\n          <div class="card" [ngStyle]="{\'background-image\':\' url(http://wecheck.co.za/rewards/index.php/api/get_reward_image/\' + reward._id + \')\', \'background-size\': \'cover\'}">\n\n            <div class="mask text-center">\n\n              <p class="center" style="color: white">{{reward.title}}</p>\n\n            </div>\n\n          </div>\n\n          <p class="text-center my_reward_text app_text_color">Points required : <span class="app_text_color">\n\n              {{reward.required_points}}</span></p>\n\n        </div>\n\n      </div>\n\n\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\pages\rewards\rewards.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_native_storage__["a" /* NativeStorage */], __WEBPACK_IMPORTED_MODULE_3__providers_server_response_server_response__["a" /* ServerResponseProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */]])
    ], RewardsPage);
    return RewardsPage;
}());

//# sourceMappingURL=rewards.js.map

/***/ })

});
//# sourceMappingURL=10.js.map