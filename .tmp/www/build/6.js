webpackJsonp([6],{

/***/ 715:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpPageModule", function() { return SignUpPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sign_up_page__ = __webpack_require__(743);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_module__ = __webpack_require__(405);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SignUpPageModule = (function () {
    function SignUpPageModule() {
    }
    SignUpPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sign_up_page__["a" /* SignUpPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sign_up_page__["a" /* SignUpPage */]),
                __WEBPACK_IMPORTED_MODULE_3__shared_module__["a" /* SharedModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__sign_up_page__["a" /* SignUpPage */]
            ]
        })
    ], SignUpPageModule);
    return SignUpPageModule;
}());

//# sourceMappingURL=sign-up-page.module.js.map

/***/ }),

/***/ 743:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignUpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_page_base_page__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_user_service__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_storage__ = __webpack_require__(225);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var SignUpPage = (function (_super) {
    __extends(SignUpPage, _super);
    function SignUpPage(injector, events, userService, viewCtrl, nativeStorage) {
        var _this = _super.call(this, injector) || this;
        _this.events = events;
        _this.userService = userService;
        _this.viewCtrl = viewCtrl;
        _this.nativeStorage = nativeStorage;
        _this.form = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormGroup */]({
            name: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required),
            username: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(3)]),
            email: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern('[^ @]*@[^ @]*')),
            password: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(6)]),
            confirmPassword: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(6)]),
            phonenumber: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(10)])
        });
        return _this;
    }
    SignUpPage.prototype.enableMenuSwipe = function () {
        return false;
    };
    SignUpPage.prototype.ionViewDidLoad = function () {
    };
    SignUpPage.prototype.onCancel = function () {
        this.viewCtrl.dismiss();
    };
    SignUpPage.prototype.isFieldValid = function (formControl) {
        return formControl.valid;
    };
    SignUpPage.prototype.onSubmit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var message, formData, message, user, transParams, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.form.invalid) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.getTrans('INVALID_FORM')];
                    case 1:
                        message = _a.sent();
                        return [2 /*return*/, this.showToast(message)];
                    case 2:
                        formData = Object.assign({}, this.form.value);
                        if (!(formData.password !== formData.confirmPassword)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.getTrans('PASSWORD_DOES_NOT_MATCH')];
                    case 3:
                        message = _a.sent();
                        return [2 /*return*/, this.showToast(message)];
                    case 4:
                        if (formData.email === '') {
                            delete formData.email;
                        }
                        delete formData.confirmPassword;
                        _a.label = 5;
                    case 5:
                        _a.trys.push([5, 8, , 9]);
                        return [4 /*yield*/, this.showLoadingView()];
                    case 6:
                        _a.sent();
                        return [4 /*yield*/, this.userService.create(formData)];
                    case 7:
                        user = _a.sent();
                        this.showContentView();
                        transParams = { username: user.username };
                        this.translate.get('LOGGED_IN_AS', transParams).subscribe(function (str) { return _this.showToast(str); });
                        this.nativeStorage.setItem('loginvalue', { id: user.id, name: user.name }).then(function () {
                            console.log('Stored item!');
                        }, function (error) {
                            console.error('Error storing item', error);
                        });
                        this.events.publish('user:login', user);
                        this.onCancel();
                        return [3 /*break*/, 9];
                    case 8:
                        err_1 = _a.sent();
                        this.showContentView();
                        if (err_1.code === 202) {
                            this.translate.get('USERNAME_TAKEN').subscribe(function (str) { return _this.showToast(str); });
                        }
                        else if (err_1.code === 203) {
                            this.translate.get('EMAIL_TAKEN').subscribe(function (str) { return _this.showToast(str); });
                        }
                        else if (err_1.code === 125) {
                            this.translate.get('EMAIL_INVALID').subscribe(function (str) { return _this.showToast(str); });
                        }
                        else {
                            this.translate.get('ERROR_NETWORK').subscribe(function (str) { return _this.showToast(str); });
                        }
                        return [3 /*break*/, 9];
                    case 9: return [2 /*return*/];
                }
            });
        });
    };
    SignUpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sign-up-page',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\pages\sign-up-page\sign-up-page.html"*/'<ion-header no-border>\n\n  <ion-toolbar class="transparent" color="primary">\n\n    <ion-buttons left>\n\n      <button ion-button (click)="onCancel()">\n\n        <span showWhen="ios,core" style="color: white !important">{{ "CLOSE" | translate }}</span>\n\n        <ion-icon name="md-close" showWhen="android,windows" ></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding text-center>\n\n\n\n  <img margin class="logo" src="./assets/img/avatar.png" width="120">\n\n\n\n  <form [formGroup]="form" (ngSubmit)="onSubmit()" novalidate>\n\n\n\n    <ion-item class="transparent">\n\n      <ion-icon [name]="isFieldValid(form.controls.name) ? \'checkmark\' : \'close\'" color="light"\n\n        item-start>\n\n      </ion-icon>\n\n      <ion-label stacked color="light">{{ "NAME" | translate }}</ion-label>\n\n      <ion-input type="text" formControlName="name"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item class="transparent">\n\n      <ion-icon [name]="isFieldValid(form.controls.username) ? \'checkmark\' : \'close\'" color="light"\n\n        item-start>\n\n      </ion-icon>\n\n      <ion-label stacked color="light">{{ "USERNAME" | translate }}</ion-label>\n\n      <ion-input type="text" formControlName="username"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item class="transparent">\n\n      <ion-icon [name]="isFieldValid(form.controls.email) ? \'checkmark\' : \'close\'" color="light"\n\n        item-start>\n\n      </ion-icon>\n\n      <ion-label stacked color="light">{{ "EMAIL" | translate }}</ion-label>\n\n      <ion-input type="email" formControlName="email" [placeholder]="\'EMAIL_VALIDATION_HELP\' | translate">\n\n      </ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item class="transparent">\n\n      <ion-icon [name]="isFieldValid(form.controls.password) ? \'checkmark\' : \'close\'" color="light"\n\n        item-start>\n\n      </ion-icon>\n\n      <ion-label stacked color="light">{{ "PASSWORD" | translate }}</ion-label>\n\n      <ion-input type="password" formControlName="password" [placeholder]="\'PASSWORD_VALIDATION_HELP\' | translate">\n\n      </ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item class="transparent">\n\n      <ion-icon [name]="isFieldValid(form.controls.confirmPassword) ? \'checkmark\' : \'close\'" color="light"\n\n        item-start>\n\n      </ion-icon>\n\n      <ion-label stacked color="light">{{ "CONFIRM_PASSWORD" | translate }}</ion-label>\n\n      <ion-input type="password" formControlName="confirmPassword"></ion-input>\n\n    </ion-item>\n\n\n\n    <ion-item class="transparent">\n\n      <ion-icon [name]="isFieldValid(form.controls.phonenumber) ? \'checkmark\' : \'close\'" color="light"\n\n        item-start>\n\n      </ion-icon>\n\n      <ion-label stacked color="light">{{ "PhoneNumber" | translate }}</ion-label>\n\n      <ion-input type="number" formControlName="phonenumber"></ion-input>\n\n    </ion-item>\n\n\n\n    \n\n\n\n    <div padding>\n\n      <button type="submit" color="light" ion-button block round>\n\n        {{ "SIGNUP" | translate }}\n\n      </button>\n\n    </div>\n\n\n\n  </form>\n\n</ion-content>'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\pages\sign-up-page\sign-up-page.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injector"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__providers_user_service__["a" /* User */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["A" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], SignUpPage);
    return SignUpPage;
}(__WEBPACK_IMPORTED_MODULE_3__base_page_base_page__["a" /* BasePage */]));

//# sourceMappingURL=sign-up-page.js.map

/***/ })

});
//# sourceMappingURL=6.js.map