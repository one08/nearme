webpackJsonp([14],{

/***/ 707:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopupimagePageModule", function() { return PopupimagePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__popupimage__ = __webpack_require__(735);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PopupimagePageModule = (function () {
    function PopupimagePageModule() {
    }
    PopupimagePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__popupimage__["a" /* PopupimagePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__popupimage__["a" /* PopupimagePage */]),
            ],
        })
    ], PopupimagePageModule);
    return PopupimagePageModule;
}());

//# sourceMappingURL=popupimage.module.js.map

/***/ }),

/***/ 735:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupimagePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the PopupimagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PopupimagePage = (function () {
    function PopupimagePage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        debugger;
        this.popimage = Object.assign({}, this.navParams.data.img);
    }
    PopupimagePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PopupimagePage');
    };
    PopupimagePage.prototype.closepopup = function () {
        this.viewCtrl.dismiss(false);
    };
    PopupimagePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-popupimage',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\pages\popupimage\popupimage.html"*/'<!--\n  Generated template for the PopupimagePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <!-- <ion-title>popupimage</ion-title> -->\n    <ion-buttons right>\n        <button ion-button (click)="closepopup()">\n          <ion-icon name="close-circle"></ion-icon>\n        </button>          \n      </ion-buttons>\n    <!-- <ion-buttons right style="background: #e8e9ec !important;">\n        <button  (click)="close()">\n          <ion-icon name="close-circle"></ion-icon>\n        </button>          \n      </ion-buttons> -->\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content >\n    <ion-scroll zooming="true" style="width: 100%; height: 100%; position: absolute;top:33%">\n        <img [src]="popimage.src" >\n      </ion-scroll>\n    \n\n\n  \n\n</ion-content>\n'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\pages\popupimage\popupimage.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["A" /* ViewController */]])
    ], PopupimagePage);
    return PopupimagePage;
}());

//# sourceMappingURL=popupimage.js.map

/***/ })

});
//# sourceMappingURL=14.js.map