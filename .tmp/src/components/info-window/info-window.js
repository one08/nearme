var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Renderer } from '@angular/core';
import { Place } from '../../providers/place-service';
import { NavController } from 'ionic-angular';
var InfoWindowComponent = (function () {
    function InfoWindowComponent(renderer, navCtrl) {
        this.renderer = renderer;
        this.navCtrl = navCtrl;
    }
    InfoWindowComponent.prototype.onImageLoad = function (imgLoader) {
        this.renderer.setElementClass(imgLoader.element, 'fade-in', true);
    };
    InfoWindowComponent.prototype.goToPlace = function () {
        this.navCtrl.push('PlaceDetailPage', { place: this.place });
    };
    __decorate([
        Input(),
        __metadata("design:type", Place)
    ], InfoWindowComponent.prototype, "place", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], InfoWindowComponent.prototype, "location", void 0);
    InfoWindowComponent = __decorate([
        Component({
            selector: 'info-window',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\components\info-window\info-window.html"*/'<ion-item no-lines class="transparent">\n\n  <ion-thumbnail item-start>\n\n      <img-loader useImg\n\n      [spinner]="false"\n\n      [src]="place.imageThumb?.url()"\n\n      fallback="./assets/img/placeholder1.png"\n\n      (load)="onImageLoad($event)">\n\n    </img-loader>\n\n  </ion-thumbnail>\n\n  <h2 class="text-medium bold no-margin ellipsis" ion-text color="darker">{{ place.title }}</h2>\n\n  <p class="text-small bold no-margin" ion-text color="dark">{{ place.category.title }}</p>\n\n  <star-rating *ngIf="place.rating"\n\n    [starType]="\'svg\'"\n\n    [size]="\'small\'"\n\n    [readOnly]="true"\n\n    [showHalfStars]="false"\n\n    [rating]="place.rating">\n\n  </star-rating>\n\n  <div item-end *ngIf="location">\n\n    <span class="text-small bold" ion-text color="primary">\n\n      {{ place.distance(location) }}\n\n    </span>\n\n  </div>\n\n</ion-item>\n\n<ion-row>\n\n  <ion-col col-12>\n\n    <button ion-button block small round color="primary" (click)="goToPlace()">\n\n      {{ \'DISCOVER_THIS_PLACE\' | translate }}\n\n    </button>\n\n  </ion-col>\n\n</ion-row>\n\n'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\components\info-window\info-window.html"*/
        }),
        __metadata("design:paramtypes", [Renderer, NavController])
    ], InfoWindowComponent);
    return InfoWindowComponent;
}());
export { InfoWindowComponent };
//# sourceMappingURL=info-window.js.map