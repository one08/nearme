var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomeTab, QRTab, SerchTab, MsgTab, MapTab } from '../';
var TabsPage = (function () {
    function TabsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tab1Root = HomeTab;
        this.tab2Root = QRTab;
        this.tab3Root = SerchTab;
        this.tab4Root = MsgTab;
        this.tab5Root = MapTab;
    }
    TabsPage = __decorate([
        Component({
            selector: 'page-tabs',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\pages\tabs\tabs.html"*/'<ion-tabs>\n\n  <ion-tab [root]="tab1Root" tabTitle="Home" tabIcon="icon-home" show=true></ion-tab>\n\n  <ion-tab [root]="tab2Root" tabTitle="QR" tabIcon="icon-QR" show=true></ion-tab>\n\n  <ion-tab [root]="tab3Root" tabTitle="Search" tabIcon="icon-search"></ion-tab>\n\n  <ion-tab [root]="tab4Root" tabTitle="Message" tabIcon="icon-mail"></ion-tab>\n\n  <ion-tab [root]="tab5Root" tabTitle="Map" tabIcon="icon-map"></ion-tab>\n\n</ion-tabs>'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\pages\tabs\tabs.html"*/,
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], TabsPage);
    return TabsPage;
}());
export { TabsPage };
//# sourceMappingURL=tabs.js.map