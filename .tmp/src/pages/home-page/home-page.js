var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component, Injector, Renderer } from '@angular/core';
import { Events } from 'ionic-angular';
import { BasePage } from '../base-page/base-page';
import Parse from 'parse';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { BrowserTab } from '@ionic-native/browser-tab';
import { Place } from '../../providers/place-service';
import { Geolocation } from '@ionic-native/geolocation';
var HomePage = (function (_super) {
    __extends(HomePage, _super);
    function HomePage(injector, events, geolocation, placeService, inAppBrowser, browserTab, renderer) {
        var _this = _super.call(this, injector) || this;
        _this.events = events;
        _this.geolocation = geolocation;
        _this.placeService = placeService;
        _this.inAppBrowser = inAppBrowser;
        _this.browserTab = browserTab;
        _this.renderer = renderer;
        _this.title = 'app works!';
        _this.result = '';
        _this.slides = [];
        _this.featuredPlaces = [];
        _this.newPlaces = [];
        _this.randomPlaces = [];
        _this.nearbyPlaces = [];
        _this.categories = [];
        _this.randomParams = {};
        _this.starCount = 5;
        return _this;
    }
    //kk
    // getHomeTimeline(){
    //   debugger;
    //   this.twitter.get(
    //     'https://api.twitter.com/1.1/statuses/home_timeline.json',
    //     {
    //       count: 5
    //     },
    //     {
    //       consumerKey: 'AKOynaFWbg2WFAFXxIeFHqX0A ',
    //       consumerSecret: 'X7yVdmVIR8ELIvX3SMhfPFcmIrzeIXcpUKZd5tCTzXuvVQ9iph '
    //     },
    //     {
    //       token: '1054367485678551042-CYnCUSVDTcMTaYGomD1hj8oipNjySQ ',
    //       tokenSecret: 'bck7A1aLkS1UbvOe2Q0CLyhkqWZyv6xhJoDRZ9lyGShUO'
    //     }
    // ).subscribe((res)=>{
    //     this.result = res.json().map(tweet => tweet.text);
    // });
    // }
    HomePage.prototype.enableMenuSwipe = function () {
        return true;
    };
    HomePage.prototype.ionViewDidLoad = function () {
        this.showLoadingView();
        this.loadData();
        this.loadNearbyPlaces();
    };
    HomePage.prototype.onImageLoad = function (imgLoader) {
        this.renderer.setElementClass(imgLoader.element, 'fade-in', true);
    };
    HomePage.prototype.onReload = function (refresher) {
        if (refresher === void 0) { refresher = null; }
        this.refresher = refresher;
        this.loadData();
        this.loadNearbyPlaces();
    };
    HomePage.prototype.loadData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var data, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, Parse.Cloud.run('getHomePageData')];
                    case 1:
                        data = _a.sent();
                        this.randomPlaces = data.randomPlaces;
                        this.newPlaces = data.newPlaces;
                        this.featuredPlaces = data.featuredPlaces;
                        this.categories = data.categories;
                        this.slides = data.slides;
                        this.onRefreshComplete();
                        this.showContentView();
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        this.showErrorView();
                        this.onRefreshComplete();
                        this.translate.get('ERROR_NETWORK')
                            .subscribe(function (str) { return _this.showToast(str); });
                        if (error_1.code === 209) {
                            this.events.publish('user:logout');
                        }
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.loadMoreRandomPlaces = function () {
        var _this = this;
        Parse.Cloud.run('getRandomPlaces').then(function (places) {
            for (var _i = 0, places_1 = places; _i < places_1.length; _i++) {
                var place = places_1[_i];
                _this.randomPlaces.push(place);
            }
            _this.onRefreshComplete();
        }, function () {
            _this.onRefreshComplete();
            _this.translate.get('ERROR_NETWORK').subscribe(function (str) { return _this.showToast(str); });
        });
    };
    HomePage.prototype.loadNearbyPlaces = function () {
        return __awaiter(this, void 0, void 0, function () {
            var options, pos, _a, err_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 3, , 4]);
                        options = {
                            enableHighAccuracy: true,
                            timeout: 20000,
                            maximumAge: 60000
                        };
                        return [4 /*yield*/, this.geolocation.getCurrentPosition(options)];
                    case 1:
                        pos = _b.sent();
                        this.location = pos.coords;
                        _a = this;
                        return [4 /*yield*/, this.placeService.load({
                                location: this.location,
                                limit: 10
                            })];
                    case 2:
                        _a.nearbyPlaces = _b.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _b.sent();
                        console.warn(err_1);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.onLoadMore = function (infiniteScroll) {
        this.infiniteScroll = infiniteScroll;
        this.randomParams.page++;
        this.loadMoreRandomPlaces();
    };
    HomePage.prototype.fixsectionClick = function () {
        this.sectionClick(this.featuredPlaces[0]);
    };
    HomePage.prototype.sectionClick = function (place, param2) {
        debugger;
        console.log(place, param2);
        // place.image._url = 'assets/img/detailpage-bg.png';
        // place.description = 'In 1868, the officially recognized year celebrated as the "birth" of Sapporo, the new Meiji government concluded that the existing administrative center of Hokkaido, which at the time was the port of Hakodate, was in an unsuitable location for defense and further development of the island. As a result, it was determined that a new capital on the Ishikari Plain should be established.';
        place.distance = 20;
        place.unit = 'km';
        // place.opened = true;
        place.fromTime = '12:00';
        place.toTime = '23:00';
        place.tag = 'Asian Food';
        place.name = place.title;
        place.starCount = 3;
        console.clear();
        console.log(place);
        this.navigateTo('PlaceDetailPage', { place: place });
    };
    HomePage.prototype.onSlideTouched = function (slide) {
        if (slide.url) {
            this.openUrl(slide.url);
        }
        else if (slide.place) {
            this.navigateTo('PlaceDetailPage', { place: slide.place });
        }
        else {
            // no match...
        }
    };
    HomePage.prototype.openUrl = function (link) {
        var _this = this;
        this.browserTab.isAvailable().then(function (isAvailable) {
            if (isAvailable) {
                _this.browserTab.openUrl(link);
            }
            else {
                _this.inAppBrowser.create(link, '_system');
            }
        }).catch(function (e) { return console.log(e); });
    };
    HomePage = __decorate([
        Component({
            selector: 'page-home-page',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\pages\home-page\home-page.html"*/'<ion-header no-border>\n\n  <ion-navbar color="primary">\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="icon-menu"></ion-icon>\n\n      <!-- <ion-icon ios="ios-menu" md="md-menu"></ion-icon> -->\n\n    </button>\n\n    <ion-title text-center>Home</ion-title>\n\n    <!-- <ion-buttons end> -->\n\n\n\n    <!--       \n\n      <button ion-button clear icon-only>\n\n        <ion-icon name="icon-filter"></ion-icon>\n\n      </button> -->\n\n\n\n\n\n\n\n    <!-- <button ion-button clear icon-only (click)="navigateTo(\'PostListPage\')">\n\n        <ion-icon name="notifications"></ion-icon>\n\n      </button>\n\n      <button ion-button clear icon-only (click)="navigateTo(\'SearchPage\')">\n\n        <ion-icon name="search"></ion-icon>\n\n      </button> -->\n\n    <!-- </ion-buttons> -->\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content #container>\n\n\n\n  <empty-view *ngIf="isErrorViewVisible" icon="alert" [text]="\'ERROR_NETWORK\' | translate">\n\n  </empty-view>\n\n\n\n  <section *ngIf="isContentViewVisible">\n\n\n\n    <!-- Top Slide List -->\n\n\n\n    <ion-slides *ngIf="slides.length" pager autoplay="4000" style="background-color:none;">\n\n      <ion-slide *ngFor="let slide of slides" (click)="onSlideTouched(slide)">\n\n        <div>\n\n          <img-loader useImg (load)="onImageLoad($event)" fallback="assets/img/placeholder-slide.png" [src]="slide.image?.url()">\n\n          </img-loader>\n\n        </div>\n\n      </ion-slide>\n\n    </ion-slides>\n\n\n\n    <!-- kk -->\n\n    <ion-row padding>\n\n      <ion-col>\n\n        <h5 text-start no-margin>\n\n          {{ \'FEATURED\' | translate }}\n\n        </h5>\n\n      </ion-col>\n\n      <ion-col col-6>\n\n        <h5 class="titlecolor" ion-text color="primary" no-margin text-end (click)="navigateTo(\'PlacesPage\', { isFeatured: true })">\n\n          {{ \'VIEW_ALL\' | translate }}\n\n        </h5>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <section class="place-row" *ngIf="featuredPlaces?.length">\n\n\n\n\n\n      <div class="div-slides" style="margin-bottom: 55px;">\n\n\n\n        <ion-slides slidesPerView="2" spaceBetween="10">\n\n          <ion-slide col-auto float-left *ngFor="let place of featuredPlaces">\n\n\n\n            <ion-col col-auto float-left (click)="sectionClick(place)">\n\n\n\n              <ion-card color="light">\n\n\n\n                <div class="image-container">\n\n                  <img-loader useImg (load)="onImageLoad($event)" [src]="place.imageThumb?.url()">\n\n                  </img-loader>\n\n                </div>\n\n\n\n                <ion-card-content text-nowrap>\n\n                  <p no-margin class="text-medium ellipsis bold">{{ place.title }}</p>\n\n                  <p class="text-medium ellipsis bold" ion-text color="accent">\n\n                    {{ place.category.title }}\n\n                  </p>\n\n                </ion-card-content>\n\n              </ion-card>\n\n\n\n            </ion-col>\n\n\n\n            <!-- <ion-card class="full-width" no-margin color="light">\n\n\n\n              <div class="image-container" (click)="fixsectionClick()">\n\n\n\n                <img src="http://68.183.119.2/parse/files/myAppId/5816421286f35558608557fb6f2248df_image.jpg" />\n\n              </div>\n\n\n\n              <ion-card-content text-nowrap>\n\n                <p no-margin class="text-medium ellipsis bold">PRIMI Seapoint</p>\n\n                <p class="text-medium ellipsis bold" ion-text color="accent">\n\n                  Food\n\n                </p>\n\n              </ion-card-content>\n\n            </ion-card> -->\n\n          </ion-slide>\n\n          <!-- <ion-slide col-auto float-left>\n\n\n\n            <ion-card class="full-width" no-margin color="light">\n\n\n\n              <div class="image-container" (click)="fixsectionClick()">\n\n\n\n                <img src="http://68.183.119.2/parse/files/myAppId/5816421286f35558608557fb6f2248df_image.jpg" />\n\n              </div>\n\n\n\n              <ion-card-content text-nowrap>\n\n                <p no-margin class="text-medium ellipsis bold">PRIMI Seapoint</p>\n\n                <p class="text-medium ellipsis bold" ion-text color="accent">\n\n                  Food\n\n                </p>\n\n              </ion-card-content>\n\n            </ion-card>\n\n          </ion-slide>\n\n          <ion-slide col-auto float-left>\n\n\n\n            <ion-card class="full-width" no-margin color="light">\n\n\n\n              <div class="image-container" (click)="fixsectionClick()">\n\n\n\n                <img src="http://68.183.119.2/parse/files/myAppId/5816421286f35558608557fb6f2248df_image.jpg" />\n\n              </div>\n\n\n\n              <ion-card-content text-nowrap>\n\n                <p no-margin class="text-medium ellipsis bold">PRIMI Seapoint</p>\n\n                <p class="text-medium ellipsis bold" ion-text color="accent">\n\n                  Food\n\n                </p>\n\n              </ion-card-content>\n\n            </ion-card>\n\n          </ion-slide>\n\n          <ion-slide col-auto float-left>\n\n\n\n            <ion-card class="full-width" no-margin color="light">\n\n\n\n              <div class="image-container" (click)="fixsectionClick()">\n\n                <img src="http://68.183.119.2/parse/files/myAppId/5816421286f35558608557fb6f2248df_image.jpg" />\n\n              </div>\n\n\n\n              <ion-card-content text-nowrap>\n\n                <p no-margin class="text-medium ellipsis bold">PRIMI Seapoint</p>\n\n                <p class="text-medium ellipsis bold" ion-text color="accent">\n\n                  Food\n\n                </p>\n\n              </ion-card-content>\n\n            </ion-card>\n\n          </ion-slide>\n\n          <ion-slide col-auto float-left>\n\n\n\n            <ion-card class="full-width" no-margin color="light">\n\n\n\n              <div class="image-container" (click)="fixsectionClick()">\n\n                <img src="http://68.183.119.2/parse/files/myAppId/5816421286f35558608557fb6f2248df_image.jpg" />\n\n              </div>\n\n\n\n              <ion-card-content text-nowrap>\n\n                <p no-margin class="text-medium ellipsis bold">PRIMI Seapoint</p>\n\n                <p class="text-medium ellipsis bold" ion-text color="accent">\n\n                  Food\n\n                </p>\n\n              </ion-card-content>\n\n            </ion-card>\n\n          </ion-slide> -->\n\n\n\n        </ion-slides>\n\n      </div>\n\n\n\n    </section>\n\n\n\n\n\n    <!-- Featured Places -->\n\n\n\n    <!-- <ion-row padding>\n\n      <ion-col >\n\n        <h5 text-start no-margin>\n\n          {{ \'FEATURED\' | translate }}\n\n        </h5>\n\n      </ion-col>\n\n      <ion-col col-6>\n\n        <h5 ion-text color="primary" no-margin text-end (click)="navigateTo(\'PlacesPage\', { isFeatured: true })">\n\n          {{ \'VIEW_ALL\' | translate }}\n\n        </h5>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <section class="place-row" *ngIf="featuredPlaces?.length">\n\n\n\n      <ion-scroll scrollX="true" direction="x" style="height: 250px">\n\n        <ion-row nowrap margin-horizontal>\n\n          <ion-col col-auto float-left *ngFor="let place of featuredPlaces" (click)="sectionClick(place)">\n\n\n\n            <ion-card color="light">\n\n\n\n              <div class="image-container">\n\n                <img-loader useImg (load)="onImageLoad($event)" [src]="place.imageThumb?.url()">\n\n                </img-loader>\n\n              </div>\n\n\n\n              <ion-card-content text-nowrap>\n\n                <p no-margin class="text-medium ellipsis bold">{{ place.title }}</p>\n\n                <p class="text-medium ellipsis bold" ion-text color="accent">\n\n                  {{ place.category.title }}\n\n                </p>\n\n              </ion-card-content>\n\n            </ion-card>\n\n\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-scroll>\n\n\n\n    </section>  -->\n\n\n\n\n\n\n\n    <!-- Nearby Places -->\n\n    <!--     \n\n    <ion-row padding *ngIf="nearbyPlaces?.length">\n\n      <ion-col col-6>\n\n        <h5 text-start no-margin>{{ \'NEARBY\' | translate }}</h5>\n\n      </ion-col>\n\n      <ion-col col-6>\n\n        <h5 ion-text color="primary" no-margin text-end (click)="navigateTo(\'PlacesPage\', { location: location })">\n\n          {{ \'VIEW_ALL\' | translate }}\n\n        </h5>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <section class="place-row" *ngIf="nearbyPlaces?.length">\n\n\n\n      <ion-scroll scrollX="true" direction="x" style="height: 250px">\n\n        <ion-row nowrap margin-horizontal>\n\n          <ion-col col-auto float-left *ngFor="let place of nearbyPlaces" (click)="sectionClick(place)">\n\n\n\n            <ion-card color="light">\n\n\n\n              <div class="image-container">\n\n                <img [src]="place.imageThumb?.url()" src-fallback="./assets/img/placeholder1.png" />\n\n              </div>\n\n\n\n              <ion-card-content text-nowrap>\n\n                <p no-margin class="text-medium ellipsis bold">{{ place.title }}</p>\n\n                <p class="text-medium ellipsis bold" ion-text color="accent">\n\n                  {{ place.category.title }}\n\n                </p>\n\n              </ion-card-content>\n\n            </ion-card>\n\n\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-scroll>\n\n\n\n    </section>\n\n\n\n    <ion-row padding>\n\n      <ion-col col-6>\n\n        <h5 text-start no-margin>{{ \'MORE_PLACES\' | translate }}</h5>\n\n      </ion-col>\n\n      <ion-col col-6>\n\n        <h5 ion-text color="primary" no-margin text-end (click)="navigateTo(\'PlacesPage\')">\n\n          {{ \'VIEW_ALL\' | translate }}\n\n        </h5>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row class="place-row">\n\n      <ion-col col-6 float-left *ngFor="let place of randomPlaces" (click)="sectionClick(place, randomPlaces)">\n\n\n\n        <ion-card class="full-width" no-margin color="light">\n\n\n\n          <div class="image-container">\n\n            <img defaultImage="./assets/img/placeholder1.png" [lazyLoad]="place.imageThumb?.url()" [scrollObservable]="container.ionScroll" />\n\n          </div>\n\n\n\n          <ion-card-content text-nowrap>\n\n            <p no-margin class="text-medium ellipsis bold">{{ place.title }}</p>\n\n            <p class="text-medium ellipsis bold" ion-text color="accent">\n\n              {{ place.category.title }}\n\n            </p>\n\n          </ion-card-content>\n\n        </ion-card>\n\n\n\n      </ion-col>\n\n    </ion-row>\n\n  </section>\n\n -->\n\n\n\n    <!-- kk -->\n\n\n\n    <ion-row padding>\n\n      <ion-col col-6>\n\n        <h5 text-start no-margin>{{ \'MORE_PLACES\' | translate }}</h5>\n\n      </ion-col>\n\n      <ion-col col-6>\n\n        <h5 class="titlecolor" ion-text color="primary" no-margin text-end (click)="navigateTo(\'PlacesPage\')">\n\n          {{ \'VIEW_ALL\' | translate }}\n\n        </h5>\n\n      </ion-col>\n\n    </ion-row>\n\n    <!-- <div class="card-container" *ngFor="let place of randomPlaces" (click)="sectionClick(place, randomPlaces)" class="imgback" style="margin-bottom:20px"> -->\n\n    <div style="margin-bottom:20px" class="card-container imgback" *ngFor="let place of randomPlaces" (click)="sectionClick(place, randomPlaces)">\n\n      <!-- [ngStyle]="{\'background-image\': \'url(\' + place.imageThumb?.url() + \')\'}"   -->\n\n      <div style="margin-bottom:20px" class="image-container imgback">\n\n        <img style="height: 100%;width: 100%;" defaultImage="./assets/img/placeholder1.png" [lazyLoad]="place.imageThumb?.url()"\n\n          [scrollObservable]="container.ionScroll" />\n\n        <div class="overlay" style="\n\n        height: inherit;\n\n        background: #2829297a;\n\n        position: absolute;\n\n        z-index: 0;\n\n        width: 100%;\n\n        top:0\n\n    "></div>\n\n      </div>\n\n\n\n\n\n\n\n\n\n      <div left class="" style="height: 100%;position: absolute;top: 0;">\n\n\n\n\n\n\n\n\n\n        <div style="height: 40%;"></div>\n\n\n\n        <div style="display: inline-flex;margin-left: 5px;width: 100%;">\n\n          <p class="restName" ion-text color="primary">\n\n            {{ place.title }}\n\n          </p>\n\n          <star-rating style="color:yellow;z-index:11;" [starType]="\'svg\'" [size]="\'medium\'" [readOnly]="true"\n\n            [showHalfStars]="false" [rating]="place.likeCount">\n\n          </star-rating>\n\n        </div>\n\n        <div class="tagName" style="display: inline-flex;margin-left: 5px">\n\n\n\n          <p class="tag distanceunit" ion-text color="primary">\n\n            {{place.category?.title}}\n\n          </p>\n\n\n\n        </div>\n\n        <div style="display: inline-flex;margin-left: 5px;width: 100%">\n\n          <img width="15" height="15" src="assets/img/maps.svg" style="z-index:11;">\n\n          <p class="distanceunit" ion-text color="primary">\n\n            5km\n\n          </p>\n\n\n\n        </div>\n\n      </div>\n\n    </div>\n\n    <!-- <div class="card-container" class="imgback1" style="margin-bottom:20px">\n\n\n\n      <div left class="" style="height: 100%;">\n\n\n\n        <div style="height: 50%;"></div>\n\n\n\n        <div style="display: inline-flex;margin-left: 5px;width: 100%;">\n\n          <p class="restName" ion-text color="primary">\n\n            SNS Restaurant\n\n          </p>\n\n          <star-rating style="color:yellow" [starType]="\'svg\'" [size]="\'medium\'" [readOnly]="true" [showHalfStars]="false"\n\n            [rating]="5">\n\n          </star-rating>\n\n        </div>\n\n        <div  class="tagName" style="display: inline-flex;margin-left: 5px">\n\n         \n\n            <p class="tag distanceunit" ion-text color="primary">\n\n              BURGERS\n\n            </p>\n\n         \n\n        </div>\n\n        <div style="display: inline-flex;margin-left: 5px;width: 100%">\n\n          <img width="15" height="15" src="assets/img/maps.svg">\n\n          <p class="distanceunit" ion-text color="primary">\n\n            10km\n\n          </p>\n\n\n\n        </div>\n\n      </div>\n\n    </div>\n\n\n\n    <div class="card-container" class="imgback2" style="margin-bottom:20px">\n\n\n\n      <div left class="" style="height: 100%;">\n\n\n\n        <div style="height: 50%;"></div>\n\n\n\n        <div style="display: inline-flex;margin-left: 5px;width: 100%;">\n\n          <p class="restName" ion-text color="primary">\n\n            Kailash Restaurant\n\n          </p>\n\n          <star-rating style="color:yellow" [starType]="\'svg\'" [size]="\'medium\'" [readOnly]="true" [showHalfStars]="false"\n\n            [rating]="5">\n\n          </star-rating>\n\n        </div>\n\n        <div  class="tagName" style="display: inline-flex;margin-left: 5px">\n\n         \n\n            <p class="tag distanceunit" ion-text color="primary">\n\n              BURGERS\n\n            </p>\n\n         \n\n        </div>\n\n        <div style="display: inline-flex;margin-left: 5px;width: 100%">\n\n          <img width="15" height="15" src="assets/img/maps.svg">\n\n          <p class="distanceunit" ion-text color="primary">\n\n            15km\n\n          </p>\n\n\n\n        </div>\n\n      </div>\n\n    </div>\n\n -->\n\n\n\n\n\n    <ion-infinite-scroll (ionInfinite)="onLoadMore($event)">\n\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n\n\n\n\n\n\n  </section>\n\n\n\n  <!-- <h1>{{title}}</h1>\n\n  <button (click)="getHomeTimeline()">get/home_timeline</button>\n\n  <p>{{result}}</p> -->\n\n\n\n</ion-content>'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\pages\home-page\home-page.html"*/,
        }),
        __metadata("design:paramtypes", [Injector,
            Events,
            Geolocation,
            Place,
            InAppBrowser,
            BrowserTab,
            Renderer])
    ], HomePage);
    return HomePage;
}(BasePage));
export { HomePage };
//# sourceMappingURL=home-page.js.map