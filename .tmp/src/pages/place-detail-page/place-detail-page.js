var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { ActionSheetController, Slides } from 'ionic-angular';
import { Component, Injector, Renderer, ViewChild } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { Place } from '../../providers/place-service';
import { Preference } from '../../providers/preference';
import { CallNumber } from '@ionic-native/call-number';
import { Geolocation } from '@ionic-native/geolocation';
//import { InAppBrowser } from '@ionic-native/in-app-browser';
//import { BrowserTab } from '@ionic-native/browser-tab';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { SocialSharing } from '@ionic-native/social-sharing';
import { BasePage } from '../base-page/base-page';
import { User } from '../../providers/user-service';
import { Review } from '../../providers/review-service';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
// import { PopupimagePage } from '../popupimage/popupimage';
import { DocumentViewer } from '@ionic-native/document-viewer';
var PlaceDetailPage = (function (_super) {
    __extends(PlaceDetailPage, _super);
    function PlaceDetailPage(injector, renderer, 
        //private platform: Platform,
        placeService, modalCtrl, preference, callNumber, geolocation, 
        // private inAppBrowser: InAppBrowser,
        // private browserTab: BrowserTab,
        reviewService, launchNavigator, socialSharing, actionSheetCtrl, cameraservice, imagepick, document) {
        var _this = _super.call(this, injector) || this;
        _this.renderer = renderer;
        _this.placeService = placeService;
        _this.modalCtrl = modalCtrl;
        _this.preference = preference;
        _this.callNumber = callNumber;
        _this.geolocation = geolocation;
        _this.reviewService = reviewService;
        _this.launchNavigator = launchNavigator;
        _this.socialSharing = socialSharing;
        _this.actionSheetCtrl = actionSheetCtrl;
        _this.cameraservice = cameraservice;
        _this.imagepick = imagepick;
        _this.document = document;
        _this.images = [];
        _this.rating = 0;
        _this.isLiked = false;
        _this.isStarred = false;
        _this.openSlide = false;
        _this.tabs = [{ index: 1, name: 'Info' }, { index: 2, name: 'Media' }, { index: 3, name: 'Menu' }, { index: 4, name: 'Social' }];
        _this.tabClickedIndex = 1;
        _this.tabClickedName = 'Info';
        _this.reviews = [];
        _this.getAllSocialFeeds = function () {
            var initialQuery = "#marvel";
            initialQuery = initialQuery.replace(" ", "");
            var queryTags = initialQuery.split(",");
            $('.social-feed-container').socialfeed({
                // FACEBOOK
                facebook: {
                    accounts: ['@teslamotors', '!teslamotors'],
                    limit: 2,
                    access_token: '150849908413827|a20e87978f1ac491a0c4a721c961b68c'
                },
                // GOOGLEPLUS
                google: {
                    accounts: queryTags,
                    limit: 0,
                    access_token: 'AIzaSyDAelFmJhg6BSUbSLe8UT7s-G53tL4_KRg'
                },
                // Twitter
                twitter: {
                    accounts: queryTags,
                    limit: 2,
                    consumer_key: 'qzRXgkI7enflNJH1lWFvujT2P',
                    consumer_secret: '8e7E7gHuTwyDHw9lGQFO73FcUwz9YozT37lEvZulMq8FXaPl8O',
                },
                // VKONTAKTE
                vk: {
                    accounts: queryTags,
                    limit: 0,
                    source: 'all'
                },
                // INSTAGRAM
                // instagram: {
                // 	accounts: queryTags,
                // 	limit: 2,
                // 	client_id: '88b4730e0e2c4b2f8a09a6184af2e218',
                // 	access_token: ''
                // },
                // GENERAL SETTINGS
                length: 400,
                show_media: true,
                media_min_width: 200,
                update_period: 5000,
                template_html: "<div class=\"social-feed-element {{? !it.moderation_passed}}hidden{{?}}\" dt-create=\"{{=it.dt_create}}\" social-feed-id = \"{{=it.id}}\"> \t\t\t<div class='content'> \t\t\t\t<a class=\"pull-left\" href=\"{{=it.author_link}}\" target=\"_blank\"> \t\t\t\t\t<img class=\"media-object\" src=\"{{=it.author_picture}}\"> \t\t\t\t</a> \t\t\t\t<div class=\"media-body\"> \t\t\t\t\t<p> \t\t\t\t\t\t<i class=\"fa fa-{{=it.social_network}}\"></i> \t\t\t\t\t\t<span class=\"author-title\">{{=it.author_name}}</span> \t\t\t\t\t\t<span class=\"muted pull-right\"> {{=it.time_ago}}</span>   \n\t\t\t\t\t</p> \t\t\t\t\t<div class='text-wrapper'> \t\t\t\t\t\t<p class=\"social-feed-text\">{{=it.text}} <a href=\"{{=it.link}}\" target=\"_blank\" class=\"read-button\">read more</a></p> \t\t\t\t\t</div> \t\t\t\t</div> \t\t\t</div> \t\t\t{{=it.attachment}} \t\t</div>",
                date_format: "ll",
                date_locale: "en",
                moderation: function (content) {
                    return (content.text) ? content.text.indexOf('fuck') == -1 : true;
                },
                callback: function () {
                    console.log("All posts collected!");
                }
            });
        };
        _this.place = _this.navParams.get('place');
        _this.unit = _this.preference.unit;
        _this.images = [];
        _this.gallery = [{
                src: 'assets/img/home-item-1.jpeg'
            },
            {
                src: 'assets/img/home-item-2.jpg'
            }];
        if (_this.place) {
            _this.rating = _this.place.rating;
            _this.loadLocation();
            if (User.getCurrentUser()) {
                _this.checkIfIsLiked();
                _this.checkIfIsStarred();
            }
            _this.loadReviews();
            // this.images.push({
            //   _url: 'assets/img/detailpage-bg.png',
            //   location_desc: 'In 1868, the officially recognized year celebrated as the "birth" of Sapporo, the new Meiji government concluded that the existing administrative center of Hokkaido, which at the time was the port of Hakodate, was in an unsuitable location for defense and further development of the island. As a result, it was determined that a new capital on the Ishikari Plain should be established.'
            // });
            if (_this.place.image) {
                _this.images.push(_this.place.image);
                console.log('images', _this.images);
            }
            /*
                  
                  if (this.place.imageTwo) {
                    this.images.push(this.place.imageTwo);
                  }
            
                  if (this.place.imageThree) {
                    this.images.push(this.place.imageThree);
                  }
            
                  if (this.place.imageFour) {
                    this.images.push(this.place.imageFour);
                  }
                  */
        }
        return _this;
    }
    PlaceDetailPage.prototype.tabclicked = function (index) {
        debugger;
        this.tabClickedIndex = index;
        this.tabClickedName = this.tabs[index - 1].name;
        this.openSlide = false;
        debugger;
        if (index == 4) {
            this.getAllSocialFeeds();
        }
    };
    PlaceDetailPage.prototype.enableMenuSwipe = function () {
        return false;
    };
    PlaceDetailPage.prototype.ionViewDidLoad = function () {
    };
    PlaceDetailPage.prototype.onImageLoad = function (imgLoader) {
        this.renderer.setElementClass(imgLoader.element, 'fade-in', true);
    };
    PlaceDetailPage.prototype.checkIfIsLiked = function () {
        return __awaiter(this, void 0, void 0, function () {
            var isLiked, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.placeService.isLiked(this.place)];
                    case 1:
                        isLiked = _a.sent();
                        this.isLiked = isLiked;
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        console.warn(err_1.message);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PlaceDetailPage.prototype.checkIfIsStarred = function () {
        return __awaiter(this, void 0, void 0, function () {
            var isStarred, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.placeService.isStarred(this.place)];
                    case 1:
                        isStarred = _a.sent();
                        this.isStarred = isStarred;
                        return [3 /*break*/, 3];
                    case 2:
                        err_2 = _a.sent();
                        console.warn(err_2.message);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PlaceDetailPage.prototype.loadLocation = function () {
        return __awaiter(this, void 0, void 0, function () {
            var options, pos, err_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        options = {
                            enableHighAccuracy: true,
                            timeout: 20000,
                            maximumAge: 60000
                        };
                        return [4 /*yield*/, this.geolocation.getCurrentPosition(options)];
                    case 1:
                        pos = _a.sent();
                        this.location = pos.coords;
                        return [3 /*break*/, 3];
                    case 2:
                        err_3 = _a.sent();
                        console.warn(err_3);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PlaceDetailPage.prototype.loadReviews = function () {
        return __awaiter(this, void 0, void 0, function () {
            var reviews, err_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.reviewService.load({ place: this.place, limit: 5 })];
                    case 1:
                        reviews = _a.sent();
                        this.reviews = reviews;
                        return [3 /*break*/, 3];
                    case 2:
                        err_4 = _a.sent();
                        console.warn(err_4.message);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PlaceDetailPage.prototype.openSignUpModal = function () {
        this.navigateTo('SignInPage');
    };
    PlaceDetailPage.prototype.openAddReviewModal = function () {
        var modal = this.modalCtrl.create('AddReviewPage', { place: this.place });
        modal.present();
    };
    PlaceDetailPage.prototype.onLike = function () {
        debugger;
        if (User.getCurrentUser()) {
            this.isLiked = true;
            this.placeService.like(this.place);
            this.showToast('Liked');
        }
        else {
            this.openSignUpModal();
        }
    };
    PlaceDetailPage.prototype.onRate = function () {
        if (User.getCurrentUser()) {
            this.openAddReviewModal();
        }
        else {
            this.openSignUpModal();
        }
    };
    PlaceDetailPage.prototype.onShare = function () {
        return __awaiter(this, void 0, void 0, function () {
            var err_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.socialSharing.share(this.place.title, null, null, this.place.website)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        err_5 = _a.sent();
                        console.warn(err_5);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    PlaceDetailPage.prototype.onCall = function () {
        return __awaiter(this, void 0, void 0, function () {
            var err_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.place.phone)
                            return [2 /*return*/];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.callNumber.callNumber(this.place.phone, true)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        err_6 = _a.sent();
                        console.warn(err_6);
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    PlaceDetailPage.prototype.openUberUrl = function () {
        var options = {
            start: '',
            app: this.launchNavigator.APP.UBER,
        };
        this.launchNavigator.navigate(this.place.title, options).then(function (success) {
            console.log(success);
        }, function () {
            console.log('Error launching navigator');
        });
    };
    PlaceDetailPage.prototype.goToMap = function () {
        return __awaiter(this, void 0, void 0, function () {
            var googleMaps, appleMaps, isGoogleMapsAvailable, isAppleMapsAvailable, app, options, destination, err_7;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        googleMaps = this.launchNavigator.APP.GOOGLE_MAPS;
                        appleMaps = this.launchNavigator.APP.APPLE_MAPS;
                        return [4 /*yield*/, this.launchNavigator.isAppAvailable(googleMaps)];
                    case 1:
                        isGoogleMapsAvailable = _a.sent();
                        return [4 /*yield*/, this.launchNavigator.isAppAvailable(appleMaps)];
                    case 2:
                        isAppleMapsAvailable = _a.sent();
                        app = null;
                        if (isGoogleMapsAvailable) {
                            app = this.launchNavigator.APP.GOOGLE_MAPS;
                        }
                        else if (isAppleMapsAvailable) {
                            app = this.launchNavigator.APP.APPLE_MAPS;
                        }
                        else {
                            app = this.launchNavigator.APP.USER_SELECT;
                        }
                        options = {
                            app: app
                        };
                        destination = [
                            this.place.location.latitude,
                            this.place.location.longitude
                        ];
                        return [4 /*yield*/, this.launchNavigator.navigate(destination, options)];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        err_7 = _a.sent();
                        console.warn(err_7);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    PlaceDetailPage.prototype.goToReviews = function () {
        this.navigateTo('ReviewsPage', this.place);
    };
    PlaceDetailPage.prototype.showPopup = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Pick Image',
            buttons: [{
                    text: 'Take Image',
                    handler: function () {
                        _this.takePicture();
                    }
                },
                {
                    text: 'From Gallery',
                    handler: function () {
                        //**************** */
                        _this.openImagePicker();
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        // console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    PlaceDetailPage.prototype.onClickGallery = function (i) {
        this.slides.slideTo(i);
        this.openSlide = true;
        //kk
        debugger;
        // let obj:any=i;
        // const modal =this.modalCtrl.create(PopupimagePage,{img:obj})
        // modal.present();
    };
    PlaceDetailPage.prototype.off = function () {
        this.openSlide = false;
    };
    PlaceDetailPage.prototype.takePicture = function () {
        var options = {
            quality: 100,
            correctOrientation: true
        };
        this.cameraservice.getPicture(options).then(function (data) {
            console.log(data);
        }, function () {
        });
    };
    PlaceDetailPage.prototype.openImagePicker = function () {
        var options = {
            maximumImagesCount: 1,
        };
        this.imagepick.getPictures(options)
            .then(function (results) {
            if (results != '') {
                results.forEach(function (item) {
                    console.log(item);
                    /* window.resolveLocalFileSystemURL(item, (fileEntry) => {
                      fileEntry.getMetadata((metadata) => {
                        console.log('size', metadata.size);
                        if (metadata.size > 20971520) {
                          this.remotService.dismissLoader();
                          this.remotService.presentToast(' Please upload a file with size less than: ' + 20 + "MB");
                        } else {
                          this.remotService.dismissLoader();
                          this.reduceImages(results).then(() => {
                            if (this.changeimageType == 'profile')
                              this.saveProfileImage();
                            else
                              this.addStatusImage();
                          });
        
                          //this.saveImageToArrayBypath(item);
                        }
                      });
                    }); */
                });
            }
        }, function () {
            //console.log(err)
        });
    };
    PlaceDetailPage.prototype.open = function () {
        return __awaiter(this, void 0, void 0, function () {
            var options, baseUrl;
            return __generator(this, function (_a) {
                // this.tabClickedIndex = index;
                // this.tabClickedName = this.tabs[index - 1].name;
                // this.openSlide = false;
                debugger;
                options = {
                    title: 'My PDF'
                };
                baseUrl = location.href.split("index.html#");
                this.document.viewDocument(baseUrl[0] + 'assets/pdf/sample.pdf', 'application/pdf', options);
                return [2 /*return*/];
            });
        });
    };
    __decorate([
        ViewChild(Slides),
        __metadata("design:type", Slides)
    ], PlaceDetailPage.prototype, "slides", void 0);
    PlaceDetailPage = __decorate([
        Component({
            selector: 'page-place-detail-page',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\pages\place-detail-page\place-detail-page.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n\n    <ion-title class=\'title-text\'>{{ place?.title }}</ion-title>\n\n    <ion-buttons end>\n\n      <button ion-button (click)="onShare()">\n\n        <ion-icon name="md-share" class="shareclass"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content >\n\n\n\n \n\n\n\n  <ion-card class="tab-card" color="primary">\n\n    <ion-card-content>\n\n\n\n      <ion-row>\n\n        <ion-col col-3 text-center class="tabcontainer" [ngClass]="{\'tabcontainerActive\': tabClickedIndex === 1}"\n\n          tapabble (click)="tabclicked(1)">\n\n          <p class="tab-bold" ion-text>Info</p>\n\n        </ion-col>\n\n\n\n        <ion-col col-3 text-center tapabble class="tabcontainer" [ngClass]="{\'tabcontainerActive\': tabClickedIndex === 2}"\n\n          (click)="tabclicked(2)">\n\n          <p class="tab-bold" ion-text>Media</p>\n\n        </ion-col>\n\n\n\n        <ion-col col-3 text-center tapabble class="tabcontainer" [ngClass]="{\'tabcontainerActive\': tabClickedIndex === 3}"\n\n          (click)="tabclicked(3)">\n\n          <p class="tab-bold" ion-text>Menu</p>\n\n        </ion-col>\n\n\n\n        <ion-col col-3 text-center tapabble class="tabcontainer" [ngClass]="{\'tabcontainerActive\': tabClickedIndex === 4}"\n\n          (click)="tabclicked(4)">\n\n          <p class="tab-bold" ion-text>Social</p>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n    </ion-card-content>\n\n  </ion-card>\n\n\n\n  <div *ngIf="tabClickedIndex === 4">\n\n    \n\n    <div class="social-feed-container">\n\n\n\n    </div>\n\n\n\n  </div>\n\n\n\n\n\n  <div *ngIf="tabClickedIndex === 3">\n\n    \n\n    <button class="btnk" ion-button round (click)="open()"> PDf </button>\n\n\n\n  </div>\n\n\n\n  <div *ngIf="tabClickedIndex === 2" [class.background]="openSlide" (tap)="off()">\n\n    <div class="row divider-row">\n\n      <div class="col divider-item">\n\n        Gallery: <span class="count">({{gallery.length}} photos)</span>\n\n      </div>\n\n    </div>\n\n    <ion-grid>\n\n      <ion-row>\n\n        <ion-col col-3 *ngFor="let pic of gallery; let i = index">\n\n          <img [src]="pic.src" (click)="onClickGallery(i)">\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n    <!--  <div class="gallery-grid-view">\n\n      <ion-gallery ion-gallery-items="gallery" ion-gallery-row="4" ion-item-action="itemAction(item)"></ion-gallery>\n\n    </div> -->\n\n    <div class="col col-40 add-photo-btn text-right" style="margin-left:10px" (tap)="showPopup()">\n\n      <span>Add photo</span>\n\n      <!-- <img src="assets/img/icon-camera.png" /> -->\n\n      <ion-icon ios="ios-camera" md="md-camera"></ion-icon>\n\n    </div>\n\n    <div class="row divider-row">\n\n      <div class="col divider-item">\n\n        User Gallery: <span class="count">({{gallery.length}} photos)</span>\n\n      </div>\n\n    </div>\n\n    \n\n      <ion-grid>\n\n        <ion-row>\n\n          <ion-col class="col" col-3>\n\n            <img src="assets/img/home-item-1.jpeg" class="dummy thumb-img" imageViewer/>\n\n          </ion-col>\n\n          <ion-col class="col" col-3>\n\n            <img src="assets/img/1.jpg" class="dummy" imageViewer/>\n\n          </ion-col>\n\n          <ion-col class="col" col-3>\n\n            <img src="assets/img/2.jpg" class="dummy" imageViewer/>\n\n          </ion-col>\n\n          <ion-col class="col" col-3>\n\n            <img src="assets/img/home-item-2.jpg" class="dummy" imageViewer>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-grid>\n\n\n\n    <div class="row divider-row">\n\n      <div class="col col-60 divider-item">\n\n        Video: <span class="count">(Video)</span>\n\n      </div>\n\n    </div>\n\n    <iframe width="100%" style="padding:3%" height="315"  src="https://www.youtube.com/embed/MLleDRkSuvk"\n\n      frameborder="0" allowfullscreen></iframe>\n\n\n\n    \n\n  </div>\n\n\n\n  <div>\n\n\n\n  <div *ngIf="tabClickedIndex === 1" parallax-header>\n\n\n\n\n\n    <ion-slides >\n\n      <ion-slide *ngFor="let image of images">\n\n        <div class="img-container">\n\n          <div class="overlay" style="\n\n          height: 100%;\n\n          background: #2829297a;\n\n          position: absolute;\n\n          z-index: 101;\n\n          width: 100%;\n\n      "></div>\n\n     \n\n\n\n       <div >\n\n         \n\n          <img-loader [src]="image?._url" style="position: absolute" class="header-image" (load)="onImageLoad($event)"></img-loader>\n\n        </div>\n\n        </div>\n\n      </ion-slide>\n\n    </ion-slides>\n\n\n\n    <div class="card-container">\n\n\n\n      <div right top class="subscribeButton">\n\n        <div (click)="onLike()">\n\n          <img width="100" src="assets/img/detailpage-subscribe.png">\n\n        </div>\n\n      </div>\n\n\n\n      <div right top class="messengerButton">\n\n        <div (click)="onMessengerClick()">\n\n          <img width="40" height="40" src="assets/img/detailpage-messenger.png">\n\n        </div>\n\n      </div>\n\n\n\n      <div left class="ratingLocationContainer" >\n\n        <div class="leftdiv" style="width: 100%;">\n\n          <div style="display: inline-flex;margin-left: 5px">\n\n            <img width="15" height="15" src="assets/icon/checked.svg">\n\n\n\n            <p class="offersReward" ion-text color="primary">\n\n              Offers Reward\n\n            </p>\n\n          </div>\n\n\n\n          <div style="height: 50px;"></div>\n\n\n\n          <div style="display: inline-flex;margin-left: 5px;width: 100%;">\n\n            <p class="restName" ion-text color="primary">\n\n              {{ place.name }}\n\n            </p>\n\n            <star-rating style="color:yellow" *ngIf="place.starCount > 0" [starType]="\'svg\'" [size]="\'medium\'"\n\n              [readOnly]="true" [showHalfStars]="false" [rating]="4"> {{place.starCount}}\n\n            </star-rating>\n\n          </div>\n\n\n\n          <div style="display: inline-flex;margin-left: 5px;width: 100%;">\n\n            <div class="tagName">\n\n              <p class="tag distanceunit" ion-text color="primary" style="margin-left:-4px">\n\n                {{ place.category?.title }}\n\n              </p>\n\n            </div>\n\n          </div>\n\n\n\n          <div style="display: inline-flex;margin-left: 5px;">\n\n            <!--  <img width="15" height="15" src="assets/img/detailpage-locationwhite.png"> -->\n\n            <img width="15" height="15" src="assets/img/maps.svg">\n\n            <p class="distanceunit" ion-text color="primary">\n\n              {{ place.distance}} {{place.unit }}\n\n            </p>\n\n            <span style="margin-left: 15px;"></span>\n\n            <img width="15" height="15" src="assets/img/detailpage-open.png">\n\n            <p class="distanceunit" ion-text color="primary">\n\n              {{ place.fromTime}} to {{place.toTime }}\n\n            </p>\n\n          </div>\n\n        </div>\n\n      </div>\n\n\n\n      \n\n\n\n      <ion-card class="shadow radius-top card-top descriptionBox" color="primary">\n\n        <ion-card-content>\n\n\n\n          <p class="descriptionText" margin-top ion-text color="dark">{{ place?.description }}</p>\n\n\n\n          <ion-row>\n\n            <ion-col col-4 text-center tapabble [class.disabled]="!place?.phone" (click)="onCall()">\n\n              <div>\n\n                <img class="imagebutton" src="assets/img/detailpage-call.png">\n\n              </div>\n\n              <p class="text-bold" ion-text>{{ \'CALL\' | translate }}</p>\n\n            </ion-col>\n\n\n\n            <ion-col col-4 text-center tapabble (click)="goToMap()">\n\n              <div>\n\n                <img class="imagebutton" src="assets/img/detailpage-navigation.png">\n\n              </div>\n\n              <p class="text-bold" ion-text>{{ \'GETDIRECTIONS\' | translate }}</p>\n\n\n\n            </ion-col>\n\n\n\n            <ion-col col-4 text-center tapabble [class.disabled]="!place?.website" (click)="openUberUrl()">\n\n              <div>\n\n                <img class="imagebutton" src="assets/img/detailpage-uber.png">\n\n              </div>\n\n              <p class="text-bold" ion-text>{{ \'UBER\' | translate }}</p>\n\n\n\n            </ion-col>\n\n          </ion-row>\n\n\n\n        </ion-card-content>\n\n      </ion-card>\n\n    \n\n    </div>\n\n\n\n    <section class="reviewSection">\n\n\n\n      <ion-row align-items-center>\n\n        <ion-col col-8>\n\n          <h5 no-margin>\n\n            <ion-icon class="text-medium reviewIcon" name="chatbubbles" color="accent"></ion-icon>\n\n            {{ \'REVIEWS\' | translate }}\n\n          </h5>\n\n        </ion-col>\n\n        <ion-col col-4 text-end>\n\n          <button class="bold" ion-button small block round color="primary" (click)="onRate()">\n\n            {{ \'POST_REVIEW\' | translate }}\n\n          </button>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <div *ngIf="!reviews.length" text-center>\n\n        <p class="text-medium" color="accent">\n\n          {{ \'EMPTY_REVIEWS\' | translate }}\n\n        </p>\n\n      </div>\n\n\n\n      <ion-list no-lines>\n\n        <div margin-bottom padding class="radius light-bg border" *ngFor="let review of reviews">\n\n          <ion-item no-padding color="light">\n\n            <ion-avatar item-start>\n\n              <img defaultImage="./assets/img/avatar.png" [lazyLoad]="review.user?.photo?.url()" [scrollObservable]="container.ionScroll" />\n\n            </ion-avatar>\n\n            <h2 class="bold no-margin">{{ review.user?.name }}</h2>\n\n            <p class="text-small no-margin" ion-text color="accent">\n\n              {{ review.createdAt | date:\'mediumDate\' }}\n\n            </p>\n\n            <star-rating [starType]="\'svg\'" [size]="\'small\'" [readOnly]="true" [showHalfStars]="false" [rating]="review.rating">\n\n            </star-rating>\n\n          </ion-item>\n\n          <ion-row>\n\n            <ion-col no-padding col-12>\n\n              <p class="text-medium bold no-margin" ion-text color="dark">{{ review.comment }}</p>\n\n            </ion-col>\n\n          </ion-row>\n\n        </div>\n\n      </ion-list>\n\n      <div text-center *ngIf="reviews.length">\n\n        <button class="bold" ion-button icon-right clear color="dark" (click)="goToReviews()">\n\n          {{ \'VIEW_ALL_REVIEWS\' | translate }}\n\n          <ion-icon name="arrow-round-forward"></ion-icon>\n\n        </button>\n\n      </div>\n\n\n\n    </section>\n\n  </div>\n\n\n\n  </div>\n\n  <!-- <div parallax-header>\n\n \n\n      <div class="header-image"></div>\n\n     \n\n      <div class="main-content">\n\n     \n\n      </div>\n\n     \n\n    </div> -->\n\n\n\n\n\n</ion-content>\n\n<ion-slides [class.slidescenter]="openSlide" >\n\n\n\n    \n\n\n\n      \n\n\n\n    <ion-slide *ngFor="let pic of gallery">\n\n        <ion-buttons end style="position: absolute;top: 0;right: 0;">\n\n            <button ion-button (click)="off()" style="background:none; box-shadow: none">\n\n              <ion-icon name="close-circle"></ion-icon>\n\n            </button>          \n\n          </ion-buttons>\n\n      <img *ngIf="openSlide" [src]="pic.src" imageViewer>\n\n    </ion-slide>\n\n  </ion-slides>'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\pages\place-detail-page\place-detail-page.html"*/
        }),
        __metadata("design:paramtypes", [Injector,
            Renderer,
            Place,
            ModalController,
            Preference,
            CallNumber,
            Geolocation,
            Review,
            LaunchNavigator,
            SocialSharing,
            ActionSheetController,
            Camera,
            ImagePicker,
            DocumentViewer])
    ], PlaceDetailPage);
    return PlaceDetailPage;
}(BasePage));
export { PlaceDetailPage };
//# sourceMappingURL=place-detail-page.js.map