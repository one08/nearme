webpackJsonp([3],{

/***/ 705:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPageModule", function() { return MapPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__map_page__ = __webpack_require__(732);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_module__ = __webpack_require__(405);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MapPageModule = (function () {
    function MapPageModule() {
    }
    MapPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__map_page__["a" /* MapPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__map_page__["a" /* MapPage */]),
                __WEBPACK_IMPORTED_MODULE_3__shared_module__["a" /* SharedModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__map_page__["a" /* MapPage */]
            ]
        })
    ], MapPageModule);
    return MapPageModule;
}());

//# sourceMappingURL=map-page.module.js.map

/***/ }),

/***/ 732:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_place_service__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_map_style__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_page_base_page__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_local_storage__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_config__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_info_window_info_window__ = __webpack_require__(412);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








var MapPage = (function (_super) {
    __extends(MapPage, _super);
    function MapPage(injector, resolver, storage, placeService, geolocation) {
        var _this = _super.call(this, injector) || this;
        _this.injector = injector;
        _this.resolver = resolver;
        _this.storage = storage;
        _this.placeService = placeService;
        _this.geolocation = geolocation;
        _this.params = {};
        _this.markers = [];
        _this.mapInitialised = false;
        _this.zoomToFit = true;
        return _this;
    }
    MapPage.prototype.enableMenuSwipe = function () {
        return false;
    };
    MapPage.prototype.ionViewDidLoad = function () {
        if (typeof google == 'undefined' || typeof google.maps == 'undefined') {
            this.loadGoogleMaps();
        }
        else {
            this.initMap();
        }
    };
    MapPage.prototype.ionViewDidEnter = function () { };
    MapPage.prototype.ionViewDidLeave = function () { };
    MapPage.prototype.loadGoogleMaps = function () {
        var _this = this;
        this.showLoadingView();
        window['mapInit'] = function () {
            _this.showContentView();
            _this.initMap();
        };
        var apiKey = __WEBPACK_IMPORTED_MODULE_6__app_app_config__["a" /* AppConfig */].GOOGLE_MAPS_API_KEY;
        var script = document.createElement('script');
        script.id = 'googleMaps';
        script.src = "https://maps.google.com/maps/api/js?key=" + apiKey + "&callback=mapInit";
        document.body.appendChild(script);
    };
    MapPage.prototype.initMap = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var mapOptions, options, pos, _a, err_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.snazzyInfoWindow = __webpack_require__(733);
                        this.geocoder = new google.maps.Geocoder();
                        this.mapInitialised = true;
                        mapOptions = {
                            styles: __WEBPACK_IMPORTED_MODULE_2__providers_map_style__["a" /* MapStyle */].light(),
                            zoom: 2,
                            center: { lat: 0, lng: 0 },
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
                        _b.label = 1;
                    case 1:
                        _b.trys.push([1, 4, , 5]);
                        options = {
                            enableHighAccuracy: true,
                            timeout: 20000,
                            maximumAge: 60000
                        };
                        return [4 /*yield*/, this.geolocation.getCurrentPosition(options)];
                    case 2:
                        pos = _b.sent();
                        this.params.location = pos.coords;
                        this.location = pos.coords;
                        _a = this.params;
                        return [4 /*yield*/, this.storage.unit];
                    case 3:
                        _a.unit = _b.sent();
                        this.loadData();
                        return [3 /*break*/, 5];
                    case 4:
                        err_1 = _b.sent();
                        console.warn(err_1);
                        this.translate.get('ERROR_LOCATION_UNAVAILABLE').subscribe(function (str) { return _this.showToast(str); });
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    MapPage.prototype.setMapOnAll = function (map) {
        this.markers.forEach(function (marker) {
            marker.setMap(map);
        });
    };
    MapPage.prototype.goToPlace = function (place) {
        this.navigateTo('PlaceDetailPage', { place: place });
    };
    MapPage.prototype.onSearchAddress = function (event) {
        var _this = this;
        if (!this.mapInitialised)
            return;
        var query = event.target.value;
        this.geocoder.geocode({ address: query }, function (results, status) {
            if (status === 'OK') {
                var target = results[0].geometry.location;
                _this.map.panTo(target);
                _this.params.location = {
                    latitude: target.lat(),
                    longitude: target.lng()
                };
                _this.zoomToFit = false;
                _this.showLoadingView();
                _this.onReload();
            }
        });
    };
    MapPage.prototype.loadData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var places, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.placeService.load(this.params)];
                    case 1:
                        places = _a.sent();
                        this.onPlacesLoaded(places);
                        this.showContentView();
                        if (!places.length) {
                            this.translate.get('EMPTY_PLACES').subscribe(function (str) { return _this.showToast(str); });
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        err_2 = _a.sent();
                        console.log(err_2);
                        this.translate.get('ERROR_NETWORK').subscribe(function (str) { return _this.showToast(str); });
                        this.showContentView();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    MapPage.prototype.onPlacesLoaded = function (places) {
        var bounds = new google.maps.LatLngBounds();
        var points = [];
        for (var _i = 0, places_1 = places; _i < places_1.length; _i++) {
            var place = places_1[_i];
            var position = new google.maps.LatLng(place.location.latitude, place.location.longitude);
            bounds.extend(position);
            var marker = new google.maps.Marker({
                icon: {
                    url: place.category.icon ? place.category.icon.url() : './assets/img/pin.png',
                    scaledSize: new google.maps.Size(32, 32),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0)
                },
                position: position,
                map: this.map,
            });
            this.markers.push(marker);
            var factory = this.resolver.resolveComponentFactory(__WEBPACK_IMPORTED_MODULE_7__components_info_window_info_window__["a" /* InfoWindowComponent */]);
            var component = factory.create(this.injector);
            component.instance.place = place;
            component.instance.location = this.params.location;
            component.changeDetectorRef.detectChanges();
            new this.snazzyInfoWindow({
                marker: marker,
                content: component.location.nativeElement,
                backgroundColor: '#fff',
                fontColor: '#444',
                fontSize: '12px',
                padding: '8px',
                wrapperClass: 'info-window-wrapper',
                showCloseButton: false,
                panOnOpen: true,
                closeWhenOthersOpen: true,
                offset: {
                    top: '-4px',
                    left: '16px'
                }
            });
            points.push(position);
        }
        if (points.length && this.zoomToFit) {
            this.map.fitBounds(bounds);
        }
    };
    MapPage.prototype.onReload = function () {
        this.setMapOnAll(null);
        this.markers = [];
        this.loadData();
    };
    MapPage.prototype.onSearchButtonTapped = function () {
        if (!this.mapInitialised)
            return;
        var bounds = this.map.getBounds();
        this.params.location = [{
                latitude: bounds.getSouthWest().lat(),
                longitude: bounds.getSouthWest().lng(),
            }, {
                latitude: bounds.getNorthEast().lat(),
                longitude: bounds.getNorthEast().lng()
            }];
        this.zoomToFit = false;
        this.showLoadingView();
        this.onReload();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
    ], MapPage.prototype, "mapElement", void 0);
    MapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-map-page',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\pages\map-page\map-page.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ "MAP" | translate }}</ion-title>\n\n  </ion-navbar>\n\n  <ion-toolbar color="primary">\n\n    <ion-searchbar color="black" mode="ios" style="color:black;"\n\n      placeholder="{{ \'ENTER_ADDRESS\' | translate }}"\n\n      (keyup.enter)="onSearchAddress($event)"></ion-searchbar>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <div #map class="map" id="map"></div>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <ion-toolbar color="secondary" text-center>\n\n    <button ion-button outline round color="light" (click)="onSearchButtonTapped()">\n\n      {{ "SEARCH_THIS_AREA" | translate }}\n\n    </button>\n\n  </ion-toolbar>\n\n</ion-footer>\n\n'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\pages\map-page\map-page.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injector"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ComponentFactoryResolver"],
            __WEBPACK_IMPORTED_MODULE_4__providers_local_storage__["a" /* LocalStorage */],
            __WEBPACK_IMPORTED_MODULE_1__providers_place_service__["a" /* Place */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */]])
    ], MapPage);
    return MapPage;
}(__WEBPACK_IMPORTED_MODULE_3__base_page_base_page__["a" /* BasePage */]));

//# sourceMappingURL=map-page.js.map

/***/ }),

/***/ 733:
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!function(t,e){if(true)!(__WEBPACK_AMD_DEFINE_ARRAY__ = [module,exports], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));else if("undefined"!=typeof exports)e(module,exports);else{var o={exports:{}};e(o,o.exports),t.SnazzyInfoWindow=o.exports}}(this,function(t,e){"use strict";function o(t,e){if(!(t instanceof e))throw new TypeError("Cannot call a class as a function")}function i(t,e){if(!t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!e||"object"!=typeof e&&"function"!=typeof e?t:e}function s(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function, not "+typeof e);t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}}),e&&(Object.setPrototypeOf?Object.setPrototypeOf(t,e):t.__proto__=e)}function n(t,e){t&&e&&Object.keys(e).forEach(function(o){t[o]=e[o]})}function r(t){var e={};return n(e,f),n(e,t),Object.keys(f).forEach(function(t){var o=f[t];if("object"===(void 0===o?"undefined":d(o))){var i={};n(i,o),n(i,e[t]),e[t]=i}}),e}function h(t,e){var o=/^(-{0,1}\.{0,1}\d+(\.\d+)?)[\s|\.]*(\w*)$/;if(t&&o.test(t)){var i=o.exec(t);return{value:1*i[1],units:i[3]||"px",original:t}}return e?h(e):{original:e}}function p(t,e){if(t){for(;t.firstChild;)t.removeChild(t.firstChild);e&&("string"==typeof e?t.innerHTML=e:t.appendChild(e))}}function a(t){return"top"===t?"bottom":"bottom"===t?"top":"left"===t?"right":"right"===t?"left":t}function l(t){return t.charAt(0).toUpperCase()+t.slice(1)}function c(t){if(void 0!==t&&null!==t){if(t instanceof google.maps.LatLng)return t;if(void 0!==t.lat&&void 0!==t.lng)return new google.maps.LatLng(t)}return null}Object.defineProperty(e,"__esModule",{value:!0});var _=function(){function t(t,e){for(var o=0;o<e.length;o++){var i=e[o];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}return function(e,o,i){return o&&t(e.prototype,o),i&&t(e,i),e}}(),d="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t},u={h:"0px",v:"3px",blur:"6px",spread:"0px",color:"#000"},f={placement:"top",pointer:!0,openOnMarkerClick:!0,closeOnMapClick:!0,closeWhenOthersOpen:!1,showCloseButton:!0,panOnOpen:!0,edgeOffset:{top:20,right:20,bottom:20,left:20}},m=function(t){function e(t){o(this,e);var s=i(this,(e.__proto__||Object.getPrototypeOf(e)).call(this,t));if("undefined"==typeof google)return console.warn("Snazzy Info Window: Google Maps is not defined!"),i(s);s._html=null,s._opts=r(t),s._callbacks=s._opts.callbacks||{},s._marker=s._opts.marker,s._map=s._opts.map,s._position=c(s._opts.position),s._isOpen=!1,s._listeners=[],s._marker&&s._opts.openOnMarkerClick&&s.trackListener(google.maps.event.addListener(s._marker,"click",function(){s.getMap()||s.open()}),!0),s._position&&!s._opts.offset&&(s._opts.offset={top:"0px",left:"0px"});var n=t.placement||s._opts.position;return("string"==typeof n||n instanceof String)&&(n=n.toLowerCase()),s._opts.placement="top"!==n&&"bottom"!==n&&"left"!==n&&"right"!==n?f.placement:n,n=s._opts.position,void 0===n||null===n||"string"==typeof n||n instanceof String||(s._opts.position=n),void 0!==s._opts.border&&s._opts.border!==!0||(s._opts.border={}),void 0===s._opts.pointer&&(s._opts.pointer=f.pointer),void 0!==s._opts.shadow&&s._opts.shadow!==!0||(s._opts.shadow={}),s}return s(e,t),_(e,[{key:"activateCallback",value:function(t){var e=this._callbacks[t];return e?e.apply(this):void 0}},{key:"trackListener",value:function(t,e){this._listeners.push({listener:t,persistent:e})}},{key:"clearListeners",value:function(t){this._listeners&&(this._listeners.forEach(function(e){!t&&e.persistent||(google.maps.event.removeListener(e.listener),e.listener=null)}),this._listeners=this._listeners.filter(function(t){return null!=t.listener}))}},{key:"isOpen",value:function(){return this._isOpen}},{key:"open",value:function(){var t=this.activateCallback("beforeOpen");(void 0===t||t)&&(this._marker?this.setMap(this._marker.getMap()):this._map&&this._position&&this.setMap(this._map))}},{key:"close",value:function(){var t=this.activateCallback("beforeClose");(void 0===t||t)&&(this.clearListeners(),this.setMap(null))}},{key:"destroy",value:function(){this.getMap()&&this.setMap(null),this.clearListeners(!0)}},{key:"setContent",value:function(t){this._opts.content=t,this._html&&this._html.content&&p(this._html.content,t)}},{key:"setPosition",value:function(t){this._position=c(t),this._isOpen&&this._position&&(this.draw(),this.resize(),this.reposition())}},{key:"setWrapperClass",value:function(t){if(this._html&&this._html.wrapper){var e=this._html.wrapper;e.className="si-wrapper-"+this._opts.placement,this._opts.border&&(e.className+=" si-has-border"),t&&(e.className+=" "+t)}this._opts.wrapperClass=t}},{key:"getWrapper",value:function(){return this._html?this._html.wrapper:null}},{key:"draw",value:function(){if(this.getMap()&&this._html&&(this._marker||this._position)){var t=this._opts.offset;t&&(t.left&&(this._html.wrapper.style.marginLeft=t.left),t.top&&(this._html.wrapper.style.marginTop=t.top));var e=this._opts.backgroundColor;if(e&&(this._html.contentWrapper.style.backgroundColor=e,this._opts.pointer&&(this._html.pointerBg.style["border"+l(this._opts.placement)+"Color"]=e)),this._opts.padding&&(this._html.contentWrapper.style.padding=this._opts.padding,this._opts.shadow&&(this._html.shadowFrame.style.padding=this._opts.padding)),this._opts.borderRadius&&(this._html.contentWrapper.style.borderRadius=this._opts.borderRadius,this._opts.shadow&&(this._html.shadowFrame.style.borderRadius=this._opts.borderRadius)),this._opts.fontSize&&(this._html.wrapper.style.fontSize=this._opts.fontSize),this._opts.fontColor&&(this._html.contentWrapper.style.color=this._opts.fontColor),this._opts.pointer&&this._opts.pointer!==!0&&(this._opts.shadow&&(this._html.shadowPointer.style.width=this._opts.pointer,this._html.shadowPointer.style.height=this._opts.pointer),this._html.pointerBorder&&(this._html.pointerBorder.style.borderWidth=this._opts.pointer),this._html.pointerBg.style.borderWidth=this._opts.pointer),this._opts.border){var o=0;if(void 0!==this._opts.border.width&&(o=h(this._opts.border.width,"0px"),this._html.contentWrapper.style.borderWidth=o.value+o.units),o=Math.round((this._html.contentWrapper.offsetWidth-this._html.contentWrapper.clientWidth)/2),o=h(o+"px","0px"),this._opts.pointer){var i=Math.min(this._html.pointerBorder.offsetHeight,this._html.pointerBorder.offsetWidth);i=h(i+"px","0px");var s=Math.round(o.value*(1.41421356237-1));s=Math.min(s,i.value),this._html.pointerBg.style.borderWidth=i.value-s+i.units;var n=l(a(this._opts.placement));this._html.pointerBg.style["margin"+n]=s+o.units,this._html.pointerBg.style[this._opts.placement]=-o.value+o.units}var r=this._opts.border.color;r&&(this._html.contentWrapper.style.borderColor=r,this._html.pointerBorder&&(this._html.pointerBorder.style["border"+l(this._opts.placement)+"Color"]=r))}if(this._opts.shadow){var p=this._opts.shadow,c=function(t){var e=p[t];return void 0!==e&&null!=e};if(c("h")||c("v")||c("blur")||c("spread")||c("color")){var _=h(p.h,u.h),d=h(p.v,u.v),f=h(p.blur,u.blur),m=h(p.spread,u.spread),g=p.color||u.color,v=function(t,e){return t+" "+e+" "+f.original+" "+m.original+" "+g};this._html.shadowFrame.style.boxShadow=v(_.original,d.original);var y=.7071067811865474*(_.value-d.value)+_.units,w=.7071067811865474*(_.value+d.value)+d.units;this._html.shadowPointerInner&&(this._html.shadowPointerInner.style.boxShadow=v(y,w))}this._opts.shadow.opacity&&(this._html.shadowWrapper.style.opacity=this._opts.shadow.opacity)}var b=this.getProjection().fromLatLngToDivPixel(this._position||this._marker.position);b&&(this._html.floatWrapper.style.top=Math.floor(b.y)+"px",this._html.floatWrapper.style.left=Math.floor(b.x)+"px"),this._isOpen||(this._isOpen=!0,this.resize(),this.reposition(),this.activateCallback("afterOpen"),google.maps.event.trigger(this.getMap(),"snazzy-info-window-opened",this))}}},{key:"onAdd",value:function(){var t=this;if(!this._html){var e=function(t,e){if(t&&e)for(var o=0;o<e.length;o++){var i=e[o];i&&(t.className&&(t.className+=" "),t.className+="si-"+i)}},o=function(){for(var t=arguments.length,o=Array(t),i=0;i<t;i++)o[i]=arguments[i];var s=document.createElement("div");return e(s,o),s};if(this._html={},this._html.wrapper=o(),this.setWrapperClass(this._opts.wrapperClass),this._opts.shadow&&(this._html.shadowWrapper=o("shadow-wrapper-"+this._opts.placement),this._html.shadowFrame=o("frame","shadow-frame"),this._html.shadowWrapper.appendChild(this._html.shadowFrame),this._opts.pointer&&(this._html.shadowPointer=o("shadow-pointer-"+this._opts.placement),this._html.shadowPointerInner=o("shadow-inner-pointer-"+this._opts.placement),this._html.shadowPointer.appendChild(this._html.shadowPointerInner),this._html.shadowWrapper.appendChild(this._html.shadowPointer)),this._html.wrapper.appendChild(this._html.shadowWrapper)),this._html.contentWrapper=o("frame","content-wrapper"),this._html.content=o("content"),this._opts.content&&p(this._html.content,this._opts.content),this._opts.showCloseButton){if(this._opts.closeButtonMarkup){var i=document.createElement("div");p(i,this._opts.closeButtonMarkup),this._html.closeButton=i.firstChild}else this._html.closeButton=document.createElement("button"),this._html.closeButton.setAttribute("type","button"),this._html.closeButton.innerHTML="&#215;",e(this._html.closeButton,["close-button"]);this._html.contentWrapper.appendChild(this._html.closeButton)}this._html.contentWrapper.appendChild(this._html.content),this._html.wrapper.appendChild(this._html.contentWrapper),this._opts.pointer&&(this._opts.border&&(this._html.pointerBorder=o("pointer-"+this._opts.placement,"pointer-border-"+this._opts.placement),this._html.wrapper.appendChild(this._html.pointerBorder)),this._html.pointerBg=o("pointer-"+this._opts.placement,"pointer-bg-"+this._opts.placement),this._html.wrapper.appendChild(this._html.pointerBg)),this._html.floatWrapper=o("float-wrapper"),this._html.floatWrapper.appendChild(this._html.wrapper),this.getPanes().floatPane.appendChild(this._html.floatWrapper);var s=this.getMap();this.clearListeners(),this._opts.closeOnMapClick&&this.trackListener(google.maps.event.addListener(s,"click",function(){t.close()})),this._opts.closeWhenOthersOpen&&this.trackListener(google.maps.event.addListener(s,"snazzy-info-window-opened",function(e){t!==e&&t.close()})),this._previousWidth=null,this._previousHeight=null,this.trackListener(google.maps.event.addListener(s,"bounds_changed",function(){var e=s.getDiv(),o=e.offsetWidth,i=e.offsetHeight,n=t._previousWidth,r=t._previousHeight;null!==n&&null!==r&&n===o&&r===i||(t._previousWidth=o,t._previousHeight=i,t.resize())})),this._marker&&this.trackListener(google.maps.event.addListener(this._marker,"position_changed",function(){t.draw()})),this._opts.showCloseButton&&!this._opts.closeButtonMarkup&&this.trackListener(google.maps.event.addDomListener(this._html.closeButton,"click",function(e){e.cancelBubble=!0,e.stopPropagation&&e.stopPropagation(),t.close()}));["click","dblclick","rightclick","contextmenu","drag","dragend","dragstart","mousedown","mouseout","mouseover","mouseup","touchstart","touchend","touchmove","wheel","mousewheel","DOMMouseScroll","MozMousePixelScroll"].forEach(function(e){t.trackListener(google.maps.event.addDomListener(t._html.wrapper,e,function(t){t.cancelBubble=!0,t.stopPropagation&&t.stopPropagation()}))}),this.activateCallback("open")}}},{key:"onRemove",value:function(){if(this.activateCallback("close"),this._html){var t=this._html.floatWrapper.parentElement;t&&t.removeChild(this._html.floatWrapper),this._html=null}this._isOpen=!1,this.activateCallback("afterClose")}},{key:"getMapInnerBounds",value:function(){var t=this.getMap().getDiv().getBoundingClientRect(),e={top:t.top+this._opts.edgeOffset.top,right:t.right-this._opts.edgeOffset.right,bottom:t.bottom-this._opts.edgeOffset.bottom,left:t.left+this._opts.edgeOffset.left};return e.width=e.right-e.left,e.height=e.bottom-e.top,e}},{key:"reposition",value:function(){if(this._opts.panOnOpen&&this._html){var t=this.getMapInnerBounds(),e=this._html.wrapper.getBoundingClientRect(),o=0,i=0;t.left>=e.left?o=e.left-t.left:t.right<=e.right&&(o=e.left-(t.right-e.width)),t.top>=e.top?i=e.top-t.top:t.bottom<=e.bottom&&(i=e.top-(t.bottom-e.height)),0===o&&0===i||this.getMap().panBy(o,i)}}},{key:"resize",value:function(){if(this._html){var t=this.getMapInnerBounds(),e=t.width;void 0!==this._opts.maxWidth&&(e=Math.min(e,this._opts.maxWidth)),e-=this._html.wrapper.offsetWidth-this._html.content.offsetWidth,this._html.content.style.maxWidth=e+"px";var o=t.height;void 0!==this._opts.maxHeight&&(o=Math.min(o,this._opts.maxHeight)),o-=this._html.wrapper.offsetHeight-this._html.content.offsetHeight,this._html.content.style.maxHeight=o+"px"}}}]),e}(function(){return"undefined"!=typeof google?google.maps.OverlayView:function(){}}());e.default=m,t.exports=e.default});
//# sourceMappingURL=snazzy-info-window.min.js.map


/***/ })

});
//# sourceMappingURL=3.js.map