webpackJsonp([8],{

/***/ 713:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function() { return SettingsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_page__ = __webpack_require__(741);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_module__ = __webpack_require__(405);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SettingsPageModule = (function () {
    function SettingsPageModule() {
    }
    SettingsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__settings_page__["a" /* SettingsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__settings_page__["a" /* SettingsPage */]),
                __WEBPACK_IMPORTED_MODULE_3__shared_module__["a" /* SharedModule */],
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__settings_page__["a" /* SettingsPage */]
            ]
        })
    ], SettingsPageModule);
    return SettingsPageModule;
}());

//# sourceMappingURL=settings-page.module.js.map

/***/ }),

/***/ 741:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_local_storage__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_page_base_page__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_preference__ = __webpack_require__(229);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SettingsPage = (function (_super) {
    __extends(SettingsPage, _super);
    function SettingsPage(injector, preference, localStorage, events) {
        var _this = _super.call(this, injector) || this;
        _this.preference = preference;
        _this.settings = {};
        _this.storage = localStorage;
        _this.events = events;
        _this.storage.unit.then(function (unit) { return _this.settings.unit = unit; }).catch(function (e) { return console.log(e); });
        _this.storage.mapStyle.then(function (mapStyle) { return _this.settings.mapStyle = mapStyle; }).catch(function (e) { return console.log(e); });
        _this.storage.lang.then(function (lang) { return _this.settings.lang = lang; }).catch(function (e) { return console.log(e); });
        return _this;
    }
    SettingsPage.prototype.enableMenuSwipe = function () {
        return true;
    };
    SettingsPage.prototype.ionViewDidLoad = function () {
    };
    SettingsPage.prototype.onChangeUnit = function () {
        this.storage.unit = this.settings.unit;
        this.preference.unit = this.settings.unit;
    };
    SettingsPage.prototype.onChangeMapStyle = function () {
        this.storage.mapStyle = this.settings.mapStyle;
        this.preference.mapStyle = this.settings.unit;
    };
    SettingsPage.prototype.onChangeLang = function () {
        if (this.settings.lang) {
            this.storage.lang = this.settings.lang;
            this.translate.use(this.settings.lang);
            this.preference.lang = this.settings.lang;
            this.events.publish('lang:change');
        }
    };
    SettingsPage.prototype.goToWalkthrough = function () {
        this.navigateTo('WalkthroughPage');
    };
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-settings-page',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\pages\settings-page\settings-page.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>{{ \'SETTINGS\' | translate }}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list no-lines radio-group (ionChange)="onChangeUnit()" [(ngModel)]="settings.unit">\n\n    <ion-list-header>\n\n      <p ion-text class="bold" color="primary">{{ \'DISTANCE_UNIT\' | translate }}</p>\n\n    </ion-list-header>\n\n    <ion-item color="light" no-lines>\n\n      <ion-label class="text-medium bold">Mi</ion-label>\n\n      <ion-radio value="mi"></ion-radio>\n\n    </ion-item>\n\n    <ion-item color="light" no-lines>\n\n      <ion-label class="text-medium bold">Km</ion-label>\n\n      <ion-radio value="km"></ion-radio>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <ion-list no-lines radio-group (ionChange)="onChangeLang()" [(ngModel)]="settings.lang">\n\n    <ion-list-header>\n\n      <p ion-text class="bold" color="primary">{{ \'LANGUAGE\' | translate }}</p>\n\n    </ion-list-header>\n\n    <ion-item color="light" no-lines>\n\n      <ion-label class="text-medium bold">{{ \'ENGLISH\' | translate }}</ion-label>\n\n      <ion-radio value="en"></ion-radio>\n\n    </ion-item>\n\n    <ion-item color="light" no-lines>\n\n      <ion-label class="text-medium bold">{{ \'SPANISH\' | translate }}</ion-label>\n\n      <ion-radio value="es"></ion-radio>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <div text-center>\n\n    <button ion-button round color="primary" (click)="goToWalkthrough()">\n\n      {{ \'OPEN_WALKTHROUGH\' | translate }}\n\n    </button>\n\n  </div>\n\n    \n\n</ion-content>\n\n'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\pages\settings-page\settings-page.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injector"],
            __WEBPACK_IMPORTED_MODULE_4__providers_preference__["a" /* Preference */],
            __WEBPACK_IMPORTED_MODULE_2__providers_local_storage__["a" /* LocalStorage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Events */]])
    ], SettingsPage);
    return SettingsPage;
}(__WEBPACK_IMPORTED_MODULE_3__base_page_base_page__["a" /* BasePage */]));

//# sourceMappingURL=settings-page.js.map

/***/ })

});
//# sourceMappingURL=8.js.map