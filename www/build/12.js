webpackJsonp([12],{

/***/ 709:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QrcodePageModule", function() { return QrcodePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__qrcode__ = __webpack_require__(737);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var QrcodePageModule = (function () {
    function QrcodePageModule() {
    }
    QrcodePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__qrcode__["a" /* QrcodePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__qrcode__["a" /* QrcodePage */]),
            ],
        })
    ], QrcodePageModule);
    return QrcodePageModule;
}());

//# sourceMappingURL=qrcode.module.js.map

/***/ }),

/***/ 737:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QrcodePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_server_response_server_response__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_native_storage__ = __webpack_require__(225);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the QrcodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var QrcodePage = (function () {
    function QrcodePage(navCtrl, barcodeScanner, navParams, alertCtrl, serverresp, nativeStorage) {
        this.navCtrl = navCtrl;
        this.barcodeScanner = barcodeScanner;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.serverresp = serverresp;
        this.nativeStorage = nativeStorage;
    }
    QrcodePage.prototype.ionViewDidLoad = function () {
        //(window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
        console.log('ionViewDidLoad QrcodePage');
    };
    QrcodePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.nativeStorage.getItem('loginvalue').then(function (data) {
            console.log(data);
            _this.userid = data.id;
            _this.scanbarcode();
        }, function (error) {
            // this.navCtrl.setRoot('SignInPage');
            console.error(error);
        });
    };
    QrcodePage.prototype.scanbarcode = function () {
        var _this = this;
        this.barcodeScanner.scan().then(function (barcodeData) {
            var data = (barcodeData.text);
            try {
                barcodeData = JSON.parse(barcodeData.text);
            }
            catch (e) {
                _this.unknownQRCode();
                console.log('qrcode note metch catch block', e);
            }
            var keys = barcodeData;
            if (!keys.hasOwnProperty('id') || !keys.hasOwnProperty("name") || !keys.hasOwnProperty("email") && !keys.hasOwnProperty("venue")) {
                _this.unknownQRCode();
            }
            else {
                var marchent_data = (JSON.parse(data));
                var marchent_id = marchent_data.id;
                /* var url = "http://wecheck.co.za/rewards/index.php/api/scan_qr_code?" +
                  "user_id=" + user.id + "&" +
                  "user_name=" + user.name + "&" +
                  "merchant_id=" + marchent_id; */
                var param = "user_id=" + _this.userid + "&" + "user_name=" + 'mike pablo' + "&" + "merchant_id=" + marchent_id;
                _this.serverresp.getqrcodedata(param).subscribe(function (resp) {
                    //console.log('server resp', resp.data.venue_name);
                    console.log('server resp', resp.venue_name);
                    _this.presentConfirm(resp);
                }, function (err) {
                    console.log('server err', err);
                });
            }
        }).catch(function (err) {
            console.log('Error', err);
        });
    };
    QrcodePage.prototype.unknownQRCode = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'Unknown qr code',
            message: 'Do you want to buy this book?',
            cssClass: 'alertDanger',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Rescan',
                    handler: function () {
                        _this.scanbarcode();
                    }
                }
            ]
        });
        alert.present();
    };
    QrcodePage.prototype.presentConfirm = function (resp) {
        var _this = this;
        var resp = resp;
        var alert = this.alertCtrl.create({
            title: '(' + resp.venue_name + ') Congratulations!',
            message: 'You have earned ' + resp.earned_points + ' points. \nPlease scan QR code again' + (resp.repeat_hours > 0 ? " after " + resp.repeat_hours + " hours" : "") + " to earn more points.",
            cssClass: 'alertDanger',
            buttons: [
                {
                    text: 'I Got It!',
                    handler: function () {
                        console.log('Buy clicked');
                        _this.navCtrl.push('RewardsPage');
                    }
                }
            ]
        });
        alert.present();
    };
    QrcodePage.prototype.ionViewWillLeave = function () {
    };
    QrcodePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-qrcode',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\pages\qrcode\qrcode.html"*/'<!--\n\n  Generated template for the QrcodePage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>qrcode</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n</ion-content>'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\pages\qrcode\qrcode.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_server_response_server_response__["a" /* ServerResponseProvider */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_native_storage__["a" /* NativeStorage */]])
    ], QrcodePage);
    return QrcodePage;
}());

//# sourceMappingURL=qrcode.js.map

/***/ })

});
//# sourceMappingURL=12.js.map