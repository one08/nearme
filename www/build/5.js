webpackJsonp([5],{

/***/ 717:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalkthroughPageModule", function() { return WalkthroughPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__walkthrough_page__ = __webpack_require__(746);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_module__ = __webpack_require__(405);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var WalkthroughPageModule = (function () {
    function WalkthroughPageModule() {
    }
    WalkthroughPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__walkthrough_page__["a" /* WalkthroughPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__walkthrough_page__["a" /* WalkthroughPage */]),
                __WEBPACK_IMPORTED_MODULE_3__shared_module__["a" /* SharedModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__walkthrough_page__["a" /* WalkthroughPage */]
            ]
        })
    ], WalkthroughPageModule);
    return WalkthroughPageModule;
}());

//# sourceMappingURL=walkthrough-page.module.js.map

/***/ }),

/***/ 746:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalkthroughPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_local_storage__ = __webpack_require__(228);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WalkthroughPage = (function () {
    function WalkthroughPage(navCtrl, storage, menu) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.menu = menu;
        this.menu.swipeEnable(false);
        this.storage.skipIntroPage.then(function (skipIntroPage) { return _this.skipIntroPage = skipIntroPage; });
    }
    WalkthroughPage.prototype.ionViewDidLoad = function () {
    };
    WalkthroughPage.prototype.goToHome = function () {
        this.skipIntroPage = true;
        this.storage.skipIntroPage = this.skipIntroPage;
        this.navCtrl.setRoot('HomePage');
    };
    WalkthroughPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-walkthrough-page',template:/*ion-inline-start:"D:\Development\VS\Nearme\nearme\src\pages\walkthrough-page\walkthrough-page.html"*/'<ion-content>\n\n  <ion-slides pager>\n\n\n\n    <ion-slide class="slide-1">\n\n      <div class="img-wrapper">\n\n        <img src="assets/img/slide-1.png" class="slide-image" />\n\n      </div>\n\n      <div class="caption">\n\n        <h2 class="slide-title bold" ion-text color="light">\n\n          {{ \'SLIDE_ONE\' | translate }}\n\n        </h2>\n\n      </div>\n\n    </ion-slide>\n\n\n\n    <ion-slide class="slide-2">\n\n      <div class="img-wrapper">\n\n        <img src="assets/img/slide-2.png" class="slide-image" />\n\n      </div>\n\n      <div class="caption">\n\n        <h2 class="slide-title bold" ion-text color="light">\n\n          {{ \'SLIDE_TWO\' | translate }}\n\n        </h2>\n\n      </div>\n\n    </ion-slide>\n\n\n\n    <ion-slide class="slide-3">\n\n      <div class="img-wrapper">\n\n        <img src="assets/img/slide-3.png" class="slide-image" />\n\n      </div>\n\n      <div class="caption">\n\n        <h2 class="slide-title bold" ion-text color="light">\n\n          {{ \'SLIDE_THREE\' | translate }}\n\n        </h2>\n\n      </div>\n\n    </ion-slide>\n\n\n\n    <ion-slide class="slide-4">\n\n      <div class="caption">\n\n        <h2 ion-text color="light">{{ \'SLIDE_FOUR\' | translate }}</h2>\n\n      </div>\n\n      <button ion-button large icon-right round color="light" (click)="goToHome()">\n\n        {{ "GET_STARTED" | translate }}\n\n        <ion-icon name="arrow-forward"></ion-icon>\n\n      </button>\n\n    </ion-slide>\n\n  </ion-slides>\n\n  <button class="skip-button bold" ion-button clear block color="light" (click)="goToHome()">\n\n    {{ "SKIP" | translate }}\n\n  </button>\n\n</ion-content>\n\n'/*ion-inline-end:"D:\Development\VS\Nearme\nearme\src\pages\walkthrough-page\walkthrough-page.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["t" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_local_storage__["a" /* LocalStorage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* MenuController */]])
    ], WalkthroughPage);
    return WalkthroughPage;
}());

//# sourceMappingURL=walkthrough-page.js.map

/***/ })

});
//# sourceMappingURL=5.js.map